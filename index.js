'use strict'

module.exports = {
  util: {
    passport: require('./util/passport'),
    mappings: require('./util/mappings')
  }
};