const { sumBy } = require('lodash');
const fspromises = require('fs/promises');

const CONSTANTS = {
  ERROR: {
    no_salesmaster_data: {
      key: 'no_salesmaster_data',
      msg: 'No transaction data from Sales Master.'
    },
    sales_not_match: {
      key: 'sales_not_match',
      msg: 'Sales lines not matched with Sales Master.'
    },
    sales_serial_number_missing: {
      key: 'sales_serial_number_missing',
      msg: 'Serial number is missing in the sales line.'
    },
    payment_lines_not_match: {
      key: 'payment_lines_not_match',
      msg: 'Payment lines not matched with Sales Master.'
    },
    payment_account_not_match: {
      key: 'payment_account_not_match',
      msg: 'Payment type from Master not matched in Stage.'
    },
    payment_bankaccount_invalid: {
      key: 'payment_bankaccount_invalid',
      msg: 'Bank account is invalid'
    },
    transaction_amount_not_match: {
      key: 'transaction_amount_not_match',
      msg: 'Transaction Amount not matched.'
    },
    salesman_field_empty: {
      key: 'salesman_field_empty',
      msg: 'Salesman code/name is empty'
    },
    customer_field_empty: {
      key: 'customer_field_empty',
      msg: 'Customer fields are empty'
    },
    item_uom_field_empty: {
      key: 'item_uom_field_empty',
      msg: 'Item UOM field is empty'
    }
  }
}

class StagingSalesValidator {

  constructor(_master, _interface) {
    this.contants = CONSTANTS;
    this.getSalesLines(_master, _interface);
    this.getPaymentLines(_master, _interface);
    this.paymentAccounts = this.getPaymentAccounts();
  }

  getSalesLines(_master, _interface) {
    const masterLines = (_master && _master.length) ? _master : [];
    const interfaceLines = (_interface && _interface.length) ? _interface : [];

    this.sales = {
      master: masterLines.filter(x => (x.linenumber && x.itemcode)),
      interface: interfaceLines.filter(x => (x.LINE_NUMBER && x.ITEM_CODE))
    };
  }

  getPaymentLines(_master, _interface) {
    const masterLines = (_master && _master.length) ? _master : [];
    const interfaceLines = (_interface && _interface.length) ? _interface : [];

    this.payments = {
      master: masterLines.filter(x => ((!x.linenumber || (x.linenumber == '0')) && !x.itemcode)),
      interface: interfaceLines.filter(x => ((!x.LINE_NUMBER || (x.LINE_NUMBER == '0')) && !x.ITEM_CODE))
    };
  }

  validateSaleLines() {
    this.validSaleLines = this.sales.master.length === this.sales.interface.length;
    
    if(!this.validSaleLines) {
      if(!this.sales.master.length) {
        this.saleLinesError = this.contants.ERROR.no_salesmaster_data;
      } else {
        this.saleLinesError = this.contants.ERROR.sales_not_match;
      }
    }

    return this;
  }

  validateSerialNumbers() {
    /**
     * Validate serial number only if sales lines is valid.
     */
    if(!this.validSaleLines) {
      return this;
    }

    /**
     * This means lines are matched. Proceed to validating of row values.
     * So set sales lines valid in this stage
     */
    this.validSerialNumbers = true;

    /**
     * Get serialized from master
     */
    const masterSerializedRows = this.sales.master.filter(x => ['X','x'].includes(x.serial));
    const salesInterface = this.sales.interface;

    let serialError;
    for (const serializedRow of masterSerializedRows) {
      const { oraclecode, documentnumber, linenumber, itemcode, cegidcode } = serializedRow;

      // Check if serial number in interface record have value
      const interfaceRowIsValid = salesInterface.filter(x => {
        const { INV_ORG_CODE, HEADER_ID, LINE_NUMBER, ITEM_CODE, SERIAL_NUMBER } = x;
        if(
          (INV_ORG_CODE == oraclecode) &&
          (HEADER_ID == documentnumber) &&
          (LINE_NUMBER == linenumber) &&
          (ITEM_CODE == itemcode) &&
          (SERIAL_NUMBER)
          ) {
          return x;
        }
      });
  
      if(!interfaceRowIsValid.length) {
        serialError = `Missing serial number in line number ${linenumber} with Item code ${itemcode}, Receipt no ${documentnumber} and Store ${cegidcode} (${oraclecode}) `;
        break;
      }
    }

    if(serialError) {
      this.validSerialNumbers = false;
      this.serialNumberError = this.contants.ERROR.sales_serial_number_missing;
      this.serialNumberError['msg'] = serialError;
    }

    return this;
  }

  validateSalesMan() {
    /**
     * Validate salesman only if sales & payment lines is valid.
     */
    if(!this.validSaleLines && this.validPaymentLines) {
      return this;
    }

    const lines = this.payments.interface.concat(this.sales.interface);
    const invalidLines = lines.filter(({SALESMAN_CODE, SALESMAN_NAME}) => {
      return (!SALESMAN_CODE || !SALESMAN_NAME);
    });

    if(invalidLines.length) {
      this.validSalesMan = false;
      this.salesManError = this.contants.ERROR.salesman_field_empty;
    } else {
      this.validSalesMan = true;
    }

    return this;
  }

  validateCustomerFields() {
    /**
     * Validate customer fields if sales & payment lines is valid.
     */
    if(!this.validSaleLines && this.validPaymentLines) {
      return this;
    }

    const lines = this.payments.interface.concat(this.sales.interface);
    const invalidLines = lines.filter(({CUSTOMER_NUMBER, SITE_NAME, CUST_SITE_NUM, CUSTOMER_NAME}) => {
      return (!CUSTOMER_NUMBER || !SITE_NAME || !CUST_SITE_NUM || !CUSTOMER_NAME);
    });

    if(invalidLines.length) {
      this.validCustomer = false;
      this.customerError = this.contants.ERROR.customer_field_empty;
    } else {
      this.validCustomer = true;
    }

    return this;
  }

  validateItemUOM() {
    /**
     * Validate UOM only if sales lines is valid.
     */
    if(!this.validSaleLines) {
      return this;
    }

    const lines = this.sales.interface;
    const invalidLines = lines.filter(({ITEM_UOM}) => {
      return (!ITEM_UOM);
    });

    if(invalidLines.length) {
      this.validItemUOM = false;
      this.itemUOMError = this.contants.ERROR.item_uom_field_empty;
    } else {
      this.validItemUOM = true;
    }

    return this;
  }

  validatePaymentLines() {
    this.validPaymentLines = this.payments.master.length === this.payments.interface.length;

    if(!this.validPaymentLines) {
      if(!this.payments.master.length) {
        this.paymentLinesError = this.contants.ERROR.no_salesmaster_data;
      } else {
        this.paymentLinesError = this.contants.ERROR.payment_lines_not_match;
      }
    }

    return this;
  }

  async validatePaymentAccounts() {
    if(!this.validPaymentLines) {
      this.paymentAccountError = this.contants.ERROR.payment_lines_not_match;
    }

    this.validPaymentAccount = true;
    for (const masterPayment of this.payments.master) {

      // Check if payment type needs to validate
      let shouldValidate = this.paymentAccounts.some(x => x.payment_code === masterPayment.paymenttype);

      // Check if payment type have bank account data in interface
      if(shouldValidate) {
        let interfaceData = this.payments.interface.filter(x => x.PAY_METHOD === masterPayment.oraclepaymenttype);

        // Check if payment from master is in interface
        if(!interfaceData.length) {
          this.validPaymentAccount = false;
          this.paymentAccountError = this.contants.ERROR.payment_account_not_match;
          break;
        }
        
        // Check if bank account is correct from interface
        const interfaceBankAccount = interfaceData[0]['BANK_ACCOUNT_NUM'];
        const { valid, validBankAccount } = await this.bankAccountValid(masterPayment, interfaceBankAccount);

        if(!valid) {
          this.validPaymentAccount = false;
          this.paymentAccountError = this.contants.ERROR.payment_bankaccount_invalid;
          this.paymentAccountError['msg'] = `Bank Account "${interfaceBankAccount}" is invalid. "${validBankAccount}" is in the master list.`;
          break;
        }
      }
    }
    return this;
  }

  validateTransactionAmount() {
    /**
     * Validate transaction amount only if sales & payment lines is valid.
     */
     if(!this.validSaleLines || !this.validPaymentLines) {
      return this;
    }

    this.validTransactionAmount = true;

    const getTotalSales = () => {
      let sum = 0;
      
      try {
        sum = sumBy(this.sales.interface, ({ ITEM_PRICE, VAT_AMOUNT, ITEM_QTY }) => {
          const price = parseFloat(ITEM_PRICE || 0);
          const qty = parseFloat(ITEM_QTY || 0);
          const vat = parseFloat(VAT_AMOUNT || 0);
          
          /**
           * Get the possitive value of item net price. (Because sometimes, VAT is negative. So this always get the positive value)
           */
          const itemNetPrice = Math.abs(vat) + Math.abs(price); 

          /**
           * Then get the total net amount of a row. QTY is either negative or possive. So the result will follow the QTY state.
           */
          const netAmount = (qty * itemNetPrice);

          return netAmount;
        });

        sum = parseFloat(sum || 0);
      } catch (error) {
       console.log(error) 
      }

      return Math.round(sum);
    }

    const getTotalPayment = () => {
      let sum = 0;
      
      try {
        sum = sumBy(this.payments.interface, ({ PAY_AMOUNT }) => parseFloat(PAY_AMOUNT || 0));
        sum = parseFloat(sum || 0);
      } catch (error) {
      }
      
      return Math.round(sum);
    }

    const totalSales = getTotalSales();
    const totalPayment = getTotalPayment();

    if(totalSales === totalPayment) {
      this.validTransactionAmount = true;
    } else if(Math.abs(totalPayment - totalSales) > .1) {
      this.validTransactionAmount = true;
    } else {
      this.validTransactionAmount = false;
      this.transactionAmountError = this.contants.ERROR.transaction_amount_not_match;
      this.transactionAmountError['msg'] = `${this.contants.ERROR.transaction_amount_not_match.msg}. Validation values: [Total Sales: ${totalSales}, Total Payment: ${totalPayment}]`;
    }

    return this;
  }

  getPaymentAccounts() {
    try {
      return require('@root/mockdata/payment-accounts.json');
    } catch (error) {
      console.error('Unable to load payment-accounts.json', error);
      return [];
    }
  }

  async bankAccountValid({ oraclecode, cegidcode, paymenttype }, bankAccountValue) {
    const bankAccounts = await this.getBankAccounts();
    const validAccounts = bankAccounts.filter(x => {
      if(
        (oraclecode == x?.oraclecode) &&
        (cegidcode == x?.cegidstore) &&
        (paymenttype == x?.paymentcode)
      ) {
        return x;
      }
    });
    
    if(!validAccounts.length) return false;

    const valid = (validAccounts[0]['bankaccount'] == bankAccountValue);
    
    return {valid, validBankAccount: validAccounts[0]['bankaccount']};
  }

  async getBankAccounts() {
    const path = `${process.env.APP_CEGID_BANK_ACCOUNT_FILE_PATH}/${process.env.APP_CEGID_BANK_ACCOUNT_JSON_FILE}`;

    try {
      const result = await fspromises.readFile(path, 'utf8');
      return JSON.parse(result);
    } catch (error) {
      console.error(`Unable to load ${path}`, error);
      /**
       * If file not exist or if json content is invalid json.
       * Return empty object
       */
      return [];
    }
  }

  isNoSalesMasterData() {
    try {
      return (!this.isValid() && 
        (this.saleLinesError == this.contants.ERROR.no_salesmaster_data || this.paymentLinesError == this.contants.ERROR.no_salesmaster_data)
      );
    } catch (error) {
      return false;
    }
    return this;
  }

  isValid() {
    const isValid = !!(this.validSaleLines && this.validPaymentLines && this.validPaymentAccount && this.validSerialNumbers && this.validTransactionAmount && this.validSalesMan && this.validCustomer && this.validItemUOM);
    return isValid;
  }

  finalize() {
    this.result = {
      valid: this.isValid(),
      errors: {
        sale_lines: this.saleLinesError,
        payment_lines: this.paymentLinesError,
        payment_account: this.paymentAccountError,
        transaction_amounts: this.transactionAmountError,
        serial_numbers: this.serialNumberError,
        sales_man: this.salesManError,
        customer: this.customerError,
        item_uom: this.itemUOMError
      }
    }

    return this;
  }
}

module.exports = {StagingSalesValidator};