const { ExtractJwt, Strategy } = require('passport-jwt');

const jwtStrategy = (passport, userModel) => {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = process.env.APP_SECRET_KEY;
  passport.use(new Strategy(opts, ({username}, done) => {
    userModel.findOne({username: username}, (err, user) => {
      if (user) {
        return done(null, user);
      }
      return done(null, err);
    });
  }));
};

module.exports = { jwtStrategy };
