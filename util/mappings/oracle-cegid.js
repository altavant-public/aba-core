module.exports = [
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_ITEMS_VIEW',
		tableKey: 'XX_WINCASH_ITEMS_VIEW',
		identifier: 'ITEM_CODE',
		csvname: 'ORACLE_ITEM',
		internalParentGroup: 'item',
		internalParentGroupLabel: 'Item',
		childGroup: 'item',
		meta: {
			inputReferenceLabel: 'Item code',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_ITEMS_VIEW',
		tableKey: 'XX_WINCASH_ITEMS_VIEW',
		identifier: 'ITEM_CODE',
		csvname: 'ORACLE_ITEM_PRICE',
		internalParentGroup: 'item',
		internalParentGroupLabel: 'Item',
		childGroup: 'price',
		meta: {
			hideInSelectInput: true,
			inputReferenceLabel: 'Item code',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_ITEMS_VIEW',
		tableKey: 'XX_WINCASH_ITEMS_VIEW',
		identifier: 'ITEM_CODE',
		csvname: 'ORACLE_ITEM_REFERENCING',
		internalParentGroup: 'item',
		internalParentGroupLabel: 'Item',
		childGroup: 'referencing',
		meta: {
			hideInSelectInput: true,
			inputReferenceLabel: 'Item code',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_ITEMS_VIEW',
		tableKey: 'XX_WINCASH_ITEMS_VIEW',
		identifier: 'ITEM_CODE',
		csvname: 'ORACLE_ITEM_TRAD',
		internalParentGroup: 'item',
		internalParentGroupLabel: 'Item',
		childGroup: 'trad',
		meta: {
			hideInSelectInput: true,
			inputReferenceLabel: 'Item code'
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_CUSTOMERS_VIEW',
		tableKey: 'XX_WINCASH_CUSTOMERS_VIEW',
		identifier: 'CUSTOMER_NUMBER',
		csvname: 'ORACLE_CUSTOMERS',
		internalParentGroup: 'customer',
		internalParentGroupLabel: 'Customer',
		childGroup: 'customer',
		meta: {
			inputReferenceLabel: 'Customer no.',
		}
	},
	// ORACLE_CURRENCY: {
	// 	routeGroup: 'oracle_cegid',
	// 	tableName: 'XX_CEGID_CURR_EXCH_RATES_VIEW',
	// tableKey: 'XX_CEGID_CURR_EXCH_RATES_VIEW',
	// 	identifier: 'FROM_CURRENCY, TO_CURRENCY', //TODO: Create feasibility for this
	// 	csvname: 'ORACLE_CURRENCY',
	// 	internalParentGroup: 'currency',
	// 	internalParentGroupLabel: 'currency',
	// 	childGroup: 'currency'
	// }
	{
		routeGroup: 'oracle_cegid',
		withDivision: false,
		tableName: 'XX_CEGID_MAIN_WH_QTY_VIEW',
		tableKey: 'XX_CEGID_MAIN_WH_QTY_VIEW',
		identifier: 'ITEM_CODE',
		csvname: 'ORACLE_INV',
		internalParentGroup: 'inventory',
		internalParentGroupLabel: 'Inventory',
		childGroup: 'inventory',
		meta: {
			inputReferenceLabel: 'Item code',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: false,
		tableName: 'XX_WINCASH_STAGE_ORDERS_V',
		tableKey: 'XX_WINCASH_STAGE_ORDERS_V',
		identifier: 'ORDER_NUMBER',
		csvname: 'ORACLE_PO',
		internalParentGroup: 'order',
		internalParentGroupLabel: 'Orders',
		childGroup: 'purchase_order',
		meta: {
			inputReferenceLabel: 'Order/Shipment no.',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: false,
		tableName: 'XX_WINCASH_STAGE_ORDERS_V',
		tableKey: 'XX_WINCASH_STAGE_ORDERS_V',
		identifier: 'SHIPMENT_NUM',
		csvname: 'ORACLE_TRV',
		internalParentGroup: 'order',
		internalParentGroupLabel: 'orders',
		childGroup: 'trv_order',
		meta: {
			inputReferenceLabel: 'Order/Shipment no.',
		}
	},
	{
		routeGroup: 'oracle_cegid',
		withDivision: true,
		tableName: 'XX_WINCASH_SALESPERSONS_V',
		tableKey: 'XX_WINCASH_SALESPERSONS_V',
		identifier: 'SALESREP_NUMBER',
		csvname: 'ORACLE_SALESPERSON',
		internalParentGroup: 'staff',
		internalParentGroupLabel: 'Staff',
		childGroup: 'staff',
		meta: {
			inputReferenceLabel: 'Staff no.',
		}
	}
]