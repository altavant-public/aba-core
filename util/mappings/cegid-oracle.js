module.exports = [
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_INVENTORY_VIEW',
		tableKey: 'XX_WINCASH_INVENTORY_VIEW',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_INVENTORY_ORACLE',
		internalParentGroup: 'inventory',
		internalParentGroupLabel: 'Inventory',
		childGroup: 'stock_adjustment',
		meta: {
			headerLabelPrefix: 'Header No.',
			inputReferenceLabel: 'Document No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_INVENTORY_VIEW',
		tableKey: 'XX_WINCASH_INVENTORY_VIEW',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_INVENTORY_ORACLE',
		internalParentGroup: 'inventory',
		internalParentGroupLabel: 'Inventory',
		childGroup: 'special_input',
		meta: {
			headerLabelPrefix: 'Header No.',
			inputReferenceLabel: 'Document No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_INVENTORY_VIEW',
		tableKey: 'XX_WINCASH_INVENTORY_VIEW',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_INVENTORY_ORACLE',
		internalParentGroup: 'inventory',
		internalParentGroupLabel: 'Inventory',
		childGroup: 'special_output',
		meta: {
			headerLabelPrefix: 'Header No.',
			inputReferenceLabel: 'Document No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_INVENTORY_V',
		tableKey: 'XX_WINCASH_STAGE_INVENTORY_V',
		identifier: 'DOC_NUMBER',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_S2ST_R2CW_ORACLE',
		internalParentGroup: 'transfers',
		internalParentGroupLabel: 'Transfers',
		childGroup: 'R2CWH',
		meta: {
			headerLabelPrefix: 'Doc No.',
			inputReferenceLabel: 'Document No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_INVENTORY_V',
		tableKey: 'XX_WINCASH_STAGE_INVENTORY_V',
		identifier: 'DOC_NUMBER',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_S2ST_R2CW_ORACLE',
		internalParentGroup: 'transfers',
		internalParentGroupLabel: 'Transfers',
		childGroup: 'S2SI',
		meta: {
			headerLabelPrefix: 'Doc No.',
			inputReferenceLabel: 'Document No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_DEPOSIT_V',
		tableKey: 'XX_WINCASH_STAGE_DEPOSIT_V',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_DEPOSIT_ORACLE',
		internalParentGroup: 'deposit',
		internalParentGroupLabel: 'Deposit',
		childGroup: 'deposit',
		meta: {
			headerLabelPrefix: 'Header No.',
			inputReferenceLabel: 'Receipt No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_SALES_V',
		tableKey: 'XX_WINCASH_STAGE_SALES_V',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_SALES_ORACLE',
		internalParentGroup: 'sale',
		internalParentGroupLabel: 'Sales',
		childGroup: 'sale',
		meta: {
			headerLabelPrefix: 'Header No.',
			inputReferenceLabel: 'Receipt No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_SALES_V',
		tableKey: 'XX_WINCASH_STAGE_SALES_V',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_EXCHANGE_ORACLE',
		internalParentGroup: 'sale',
		internalParentGroupLabel: 'Sales',
		childGroup: 'exchange',
		meta: {
			headerLabelPrefix: 'Header No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_SALES_V',
		tableKey: 'XX_WINCASH_STAGE_SALES_V',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_RETURN_ORACLE',
		internalParentGroup: 'sale',
		internalParentGroupLabel: 'Sales',
		childGroup: 'return',
		meta: {
			headerLabelPrefix: 'Header No.',
			transactionDateField: 'CREATION_DATE'
		}
	},
	{
		routeGroup: 'cegid_oracle',
		withDivision: true,
		tableName: 'XX_WINCASH_STAGE_SALES_V',
		tableKey: 'XX_WINCASH_STAGE_SALES_V',
		identifier: 'HEADER_ID',
		storeFieldName: 'INV_ORG_CODE',
		csvname: 'EXPORT_VOUCHER_ORACLE',
		internalParentGroup: 'sale',
		internalParentGroupLabel: 'Sales',
		childGroup: 'voucher',
		meta: {
			headerLabelPrefix: 'Header No.',
			transactionDateField: 'CREATION_DATE'
		}
	}
];