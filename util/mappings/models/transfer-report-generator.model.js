const { InboundModel } = require('@database/mongodb/model');

class TransferInterfaceReportModel {
  
  constructor(filename = '') {
    this.geneateColumns();
    this.filename = filename ? filename : `transfer_interface_report_${Date.now()}.csv`;
  }

  geneateColumns() {
    this.columns = [
      { id: 'interface_status', title: 'Status' },
      { id: 'date', title: 'Transaction date' },
      { id: 'division', title: 'Division / Operating Unit Code' },
      { id: 'doc_number', title: 'Document No.' },
      { id: 'oracle_sender_code', title: 'Oracle Sender Store' },
      { id: 'oracle_recipient_code', title: 'Oracle Recipient Store' },
      { id: 'sender_storecode', title: 'Cegid Sender Store' },
      { id: 'recipient_storecode', title: 'Cegid Recipient Store' },
      { id: 'reason', title: 'Transfer Reason' },
      { id: 'oracle_error', title: 'Oracle Error' },
      { id: 'oracle_success', title: 'Oracle Success Response' },
      { id: 'note', title: 'Notes' }
    ];
  }

  async generateRows(records) {
    this.data = [];

    for (const item of records) {
      const { data, status } = item;
      const {division, doc_number, oracle_sender_code, oracle_recipient_code, sender_storecode, recipient_storecode, reason, date} = data;

      const row = {
        interface_status: status.external == '1' ? 'Pushed' : 'Not pushed',
        date: date,
        division: division,
        doc_number: doc_number,
        oracle_sender_code: oracle_sender_code,
        oracle_recipient_code: oracle_recipient_code,
        sender_storecode: sender_storecode,
        recipient_storecode: recipient_storecode,
        reason: reason,
        oracle_error: '',
        oracle_success: '',
        note: ''
      };

      // Get inbound record
      const {payload, success} = await this.getInboundData(data);

      if(success) {
        // Tracked with response
        if(payload) {
          row['oracle_success'] = payload.response.success;
          row['oracle_error'] = payload.response.error;
        }

        /**
         * Other Notes
         */
        if(status.external == '1' && status.interface == '0' && !payload) { // Pushed to oracle but not tracked
          row['note'] = 'No tracking data but pushed in oracle. This might be an old transaction';
        } else if(status.external == '0' && status.interface == '1'  && payload) { // Pushed to oracle but not tracked
          row['note'] = 'Pushed to oracle with response. But transaction missing in their system.';
        } else if(status.external == '0' && status.interface == '0' && !payload) { // Never pushed to oracle and interface
          row['note'] = 'No tracking data at all. Transaction might not exported from Cegid.';
        }
      } else {
        // Error in mongoose query.
        row['interface_error'] = 'n/a';
        row['oracle_success'] = 'n/a';
        row['oracle_error'] = 'n/a';
      }
      
      this.data.push(row)
    }

    return this;
  }

  async getInboundData({division, doc_number, oracle_sender_code, oracle_recipient_code, reason}) {
    try {
      const query = {
        'data.OPERATING_UNIT_CODE': division,
        'data.DOC_NUMBER': doc_number,
        'data.INV_ORG_CODE': oracle_sender_code,
        'data.DEST_INV_ORG_CODE': oracle_recipient_code,
        'data.TRANSFER_REASON': reason,
      };
      const result = await InboundModel.findOne(query, 'response', {lean: true}).sort({createdAt: -1});
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }
}

module.exports = TransferInterfaceReportModel;