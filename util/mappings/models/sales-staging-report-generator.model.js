const moment = require('moment-timezone');
moment.tz.setDefault(process.env.APP_TIMEZONE);

class SalesStagingReportModel {
  
  constructor(filename = '') {
    this.geneateColumns();
    this.filename = filename ? filename : `failed_staging_for_sale_${Date.now()}.csv`;
    this.reportPath = `${process.env.APP_ARCHIVES_PATH}/ATTACHMENT`;
  }

  geneateColumns() {
    this.columns = [
      { id: 'division', title: 'Division / Operating Unit Code' },
      { id: 'oracle_store', title: 'Oracle Store Code' },
      { id: 'header_id', title: 'Document No.' },
      { id: 'errors', title: 'Errors' },
      { id: 'csv', title: 'CSV File' }
    ];
  }

  generateErrors(errors) {
    if(typeof errors !== 'object') return '';

    let errorMsg = '';

    try {
      let count = 1;
      for (const key in errors) {
        if(errors[key]) {
          errorMsg += `${count}. ${errors[key]['msg']}\n`;
          count++;
        }
      }
      return errorMsg
    } catch (error) {
      return 'unable to get errors';
    }
  }
  
  async generateRows(records, returnRow) {
    this.rows = records.map(x => {
      const rowData = x.data[0];
      const errorMeta = ('error' in x.meta) ? x.meta.error : {};

      let row = {
        key: x.key,
        valid: (typeof x.valid == 'boolean') ? x.valid : 'n/a',
        status: x.meta.status || 'n/a',
        division: rowData.OPERATING_UNIT_CODE,
        oracle_store: rowData.INV_ORG_CODE,
        header_id: rowData.HEADER_ID,
        transaction_lines_error: ('sale_lines' in errorMeta && errorMeta.sale_lines) ? errorMeta.sale_lines.msg : '',
        payment_lines_error: ('payment_lines' in errorMeta && errorMeta.payment_lines) ? errorMeta.payment_lines.msg : '',
        payment_account_error: ('payment_account' in errorMeta && errorMeta.payment_account) ? errorMeta.payment_account.msg : '',
        csv: x.meta.csvname,
        errors: this.generateErrors(errorMeta)
      };

      if(returnRow) {
        let rowData = {...x};
        rowData['createdAtText'] = moment(rowData.createdAt).format('MMM DD, YYYY hh:mm A z');
        rowData['updatedAtText'] = moment(rowData.updatedAt).format('MMM DD, YYYY hh:mm A z');
        row['row_data'] = rowData;
        
      }

      return row;
    });
  }

  async generateCSV() {
    try {
      const { CSVBuilderService } = require('@services');
      const csv = new CSVBuilderService(this.filename, this.reportPath);
      const { error } = await csv.rows(this.rows).setColumn(this.columns).generate();
      
      if(!error) {
        return {filepath: `${this.reportPath}/${this.filename}`, filename: this.filename};
      }
    } catch (error) {
      return {error: error.message};
    }
  }
}

module.exports = SalesStagingReportModel;