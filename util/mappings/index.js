const cegidoracle = require('./cegid-oracle');
const oraclecegid = require('./oracle-cegid');
const dataDictionary = require('./metadata/data-dictionary');
const CSV_NAMES = require('./metadata/csv-names');

const mappings = {};

// As objects
mappings.getAll = [].concat(cegidoracle, oraclecegid);

mappings.cegid_oracle = cegidoracle;
mappings.oracle_cegid = oraclecegid;
mappings.userTypeMeta = require('./metadata/user-type');
mappings.statusMeta = require('./metadata/status');
mappings.tableMeta = require('./metadata/oracle-tables');
mappings.tableColumnView = require('./metadata/table-column-view');
mappings.divisionList = require('./metadata/division-list');
mappings.csvNames = CSV_NAMES;

mappings.label = (key) => {
  try {
    return dataDictionary[key] ? dataDictionary[key] : key;
    
  } catch (error) {
    return key;
  }
}

mappings.getMapping = ({routeGroup, childGroup, internalParentGroup}, additionalFields = {}) => {
  const mapData = mappings[routeGroup].filter(x => {
    if(childGroup) {
      if(childGroup == x.childGroup && internalParentGroup == x.internalParentGroup) return x;
    } else {
      if(internalParentGroup == x.internalParentGroup) return x;
    }
  }).pop();

  if(mapData) {
    try {
      const tableMeta = mappings.tableMeta[mapData.tableName];
      mapData['groupBy'] = tableMeta.groupBy;

      if('tablemeta.getHeaderBy' in additionalFields) {
        mapData['getHeaderBy'] = tableMeta.getHeaderBy;
      }
      return mapData;
    } catch (error) {
      console.log(error);
    }
  }
  return false;
}

mappings.getCSVnames = (routeGroup) => {
  let list = [];
  if(routeGroup) {
    list = CSV_NAMES[routeGroup].map(x => {
      x.routeGroup = routeGroup;
      return x;
    });
  } else {
    for (const key in CSV_NAMES) {
      let items = CSV_NAMES[key].map(x => {
        x.routeGroup = key;
        return x;
      });
      list = list.concat(items);
    }
  }
  return list;
}

module.exports = mappings;