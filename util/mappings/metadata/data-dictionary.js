module.exports = {
  sale: 'Sale',
  exchange: 'Exchange',
  return: 'Return',
  voucher: 'Voucher',
  deposit: 'Deposit',
  transfers: 'Transfers',
  inventory: 'Inventory',
  cegid_oracle: 'Cegid to Oracle',
  oracle_cegid: 'Oracle to Cegid',
  R2CWH: 'Return to central warehouse',
  R2CW2:'Delivery for owners',
  S2SI: 'Shop to Shop transfer',
  S2SO: 'Shop to Shop transfer OUT',
  R2SC: 'Return to Service Center',
  INV: 'Stocktaking',
  item: 'Item',
  price: 'Price',
  referencing: 'Referencing',
  purchase_order: 'Purchase order',
  trv_order: 'Central WH to Store transfer',
  stock_adjustment: 'Stock Adjustment',
  special_input: 'Special Input',
  special_output: 'Special Output',
}