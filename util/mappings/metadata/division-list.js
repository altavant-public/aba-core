module.exports = [
  {
    "key": "LUX",
    "description": "Luxury Doha"
  },
  {
    "key": "KLD",
    "description": "Kuwait Luxury Division"
  },
  {
    "key": "DID",
    "description": "Dohatna"
  },
  {
    "key": "ISM",
    "description": "iSpot"
  },
  {
    "key": "SPG",
    "description": "Technogym"
  },
  {
    "key": "PSC",
    "description": "KSA"
  },
  {
    "key": "LNH",
    "description": "Linen House"
  },
  {
    "key": "FRT",
    "description": "Frette"
  },
  {
    "key": "RMS",
    "description": "Richard Mille Kuwait"
  }
]