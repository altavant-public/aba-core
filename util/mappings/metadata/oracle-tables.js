const isNumber = (char) => {
  if (typeof char !== 'string') {
    return false;
  }

  if (char.trim() === '') {

    return false;
  }

  return !isNaN(char);
};

module.exports = {
  // INBOUNDS
  'XX_WINCASH_INVENTORY_VIEW': {
    'table': 'XX_WINCASH_INT.XX_WINCASH_INVENTORY_VIEW',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup', 
        'store': '$data.INV_ORG_CODE',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data}) => {
      return {
        'route': 'cegid_oracle', 
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': meta.childGroup,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    getPushedDataBy: (payload) => {
      return {
        'identifier': payload.identifier,
        'meta.csvname': payload.meta.csvname,
        'meta.childGroup': payload.meta.childGroup,
        'data.REC_TYPE': payload.data.REC_TYPE,
        'data.INV_ORG_CODE': payload.data.INV_ORG_CODE
      }
    },
    unique_identifier: [
      {column: 'HEADER_ID', label: 'Header no.'}
    ],
    dataFieldAggregate: [
      {column: 'INV_ORG_CODE', label: 'Store'},
    ],
    dateFields: ['TRANSACTION_DATE'],
    filters: [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Document no.', field: 'HEADER_ID'},
      {label: 'Item code', field: 'ITEM_CODE'},
      {label: 'Serial no.', field: 'SERIAL_NUMBER'}
    ],
    'headerIdentifierOracleFields': ['OPERATING_UNIT_CODE','INV_ORG_CODE','HEADER_ID','REC_TYPE']
  },
  'XX_WINCASH_STAGE_INVENTORY_V': {
    table: 'XX_WINCASH_INT.XX_WINCASH_STAGE_INVENTORY_V',
    groupBy: {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE',
        'destinationStore': '$data.DEST_INV_ORG_CODE'
      }
    },
    groupHistoryBy: {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE',
        'destinationStore': '$data.DEST_INV_ORG_CODE',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'data.DEST_INV_ORG_CODE': data.DEST_INV_ORG_CODE,
        'data.DEST_INV_ORG_CODE': data.DEST_INV_ORG_CODE,
        'meta.childGroup': meta.childGroup
      }
    },
    getHistoryBy: ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'data.DEST_INV_ORG_CODE': data.DEST_INV_ORG_CODE,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    unique_identifier: [
      {column: 'INV_ORG_CODE', label: 'Sender Store'},
      {column: 'DEST_INV_ORG_CODE', label: 'Receipient Store'},
      {column: 'DOC_NUMBER', label: 'Document no.'}
    ],
    transferReason: {
      S2SI: 'S2SI',
      R2CWH: 'R2CWH'
    },
    dateFields: ['DOC_DATE'],
    filters: [
      {label: 'Document no.', field: 'DOC_NUMBER'},
      {label: 'Sender Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Recipient Oracle WH code', field: 'DEST_INV_ORG_CODE'},
      {label: 'Transfer reason', field: 'TRANSFER_REASON'},
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Item code', field: 'ITEM_CODE'}
    ],
    'headerIdentifierOracleFields': ['OPERATING_UNIT_CODE','INV_ORG_CODE','DOC_NUMBER','DEST_INV_ORG_CODE','TRANSFER_REASON']
  },
  'XX_WINCASH_STAGE_DEPOSIT_V': {
    'table': 'XX_WINCASH_INT.XX_WINCASH_STAGE_DEPOSIT_V',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup', 
        'store': '$data.INV_ORG_CODE', 
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'dateFields': ['TRANSACTION_DATE'],
    'filters': [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Reference no.', field: 'HEADER_ID'},
      {label: 'Journal no.', field: 'JOURNAL_ID'},
      {label: 'Receipt no.', field: 'RECEIPT_NUM'}
    ],
    'headerIdentifierOracleFields': ['OPERATING_UNIT_CODE','INV_ORG_CODE','HEADER_ID','JOURNAL_ID','RECEIPT_NUM']
  },
  'XX_WINCASH_STAGE_SALES_V': {
    table: 'XX_WINCASH_INT.XX_WINCASH_STAGE_SALES_V',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup', 
        'store': '$data.INV_ORG_CODE', 
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'cegid_oracle', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    unique_identifier: [
      {column: 'INV_ORG_CODE', label: 'Store'},
      {column: 'HEADER_ID', label: 'Header no.'}
    ],
    'filters': [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Receipt no.', field: 'HEADER_ID'},
      {label: 'Item code', field: 'ITEM_CODE'},
      {label: 'Serial no.', field: 'SERIAL_NUMBER'}
    ],
    'dateFields': ['TRANSACTION_DATE'],
    'headerIdentifierOracleFields': ['OPERATING_UNIT_CODE','INV_ORG_CODE','HEADER_ID']
  },
  // OUTBOUNDS
  'XX_WINCASH_ITEMS_VIEW': {
    'table': 'XX_WINCASH_INT.XX_WINCASH_ITEMS_VIEW_TEMP',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'comment': '$comment', 
        'identifier': '$identifier', 
        'store': '$data.INV_ORG_CODE',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment}) => {
      return {
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': 'item'
      }
    },
    'getHistoryBy': ({ identifier, comment, createdAt }) => {
      return { 
        'comment': comment,
        'identifier': identifier,
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'updateStatusBy': ({OPERATING_UNIT_CODE, INV_ORG_CODE, ITEM_CODE}, filename) => {
      const filters = {
        'identifier': {$in: !Array.isArray(ITEM_CODE) ? [ITEM_CODE] : ITEM_CODE},
        'data.OPERATING_UNIT_CODE': {$in: !Array.isArray(OPERATING_UNIT_CODE) ? [OPERATING_UNIT_CODE] : OPERATING_UNIT_CODE},
        'data.INV_ORG_CODE': {$in: !Array.isArray(INV_ORG_CODE) ? [INV_ORG_CODE] : INV_ORG_CODE}
      };

      // Generate csv file query
      try {
        if(filename.indexOf('PRICE') >= 0) {
          filters['meta.csv.price'] = filename;
        } else if(filename.indexOf('REFERENCING') >= 0) {
          filters['meta.csv.referencing'] = filename;
        } else {
          filters['meta.csv.item'] = filename;
        }
      } catch (error) {
        
      }

      return filters;
    },
    'fieldsToUpdateStatus': ['OPERATING_UNIT_CODE', 'INV_ORG_CODE', 'ITEM_CODE'],
    'filters': [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Item code', field: 'ITEM_CODE'},
      {label: 'Legacy item code', field: 'LEGACY_ITEM_CODE'},
      {label: 'Item type', field: 'ITEM_TYPE'},
      {label: 'Status', field: 'REC_STATUS'}
    ],
    'toArchiveQuery': ({ identifier }) => {
      return {
        'comment': 'item', 
        'identifier': identifier, 
        'meta.childGroup': 'item'
      }
    },
    'archiveFrom': 50
  },
  'XX_WINCASH_CUSTOMERS_VIEW': {
    'table': 'XX_WINCASH_INT.XX_WINCASH_CUSTOMERS_VIEW_TEMP',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data}) => {
      return {
        'route': 'oracle_cegid', 
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': meta.childGroup
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'oracle_cegid', 
        'identifier': identifier,
        'comment': comment,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'updateStatusBy': ({OPERATING_UNIT_CODE, CUSTOMER_NUMBER}) => {
      return {
        'identifier': {$in: !Array.isArray(CUSTOMER_NUMBER) ? [CUSTOMER_NUMBER] : CUSTOMER_NUMBER},
        'data.OPERATING_UNIT_CODE': {$in: !Array.isArray(OPERATING_UNIT_CODE) ? [OPERATING_UNIT_CODE] : OPERATING_UNIT_CODE}
      }
    },
    'fieldsToUpdateStatus': ['OPERATING_UNIT_CODE', 'CUSTOMER_NUMBER'],
    'filters': [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Customer no.', field: 'CUSTOMER_NUMBER'}
    ],
    'toArchiveQuery': ({ identifier }) => {
      return {
        'comment': 'customer', 
        'identifier': identifier, 
        'meta.childGroup': 'customer'
      }
    },
    'archiveFrom': 20
  },
  // 'XX_CEGID_CURR_EXCH_RATES_VIEW': {
    // table: 'XX_WINCASH_INT.XX_CEGID_CURR_EXCH_RATES_VIEW',
  //   groupBy: {
  //     _id: {
  //       identifier: '$identifier'
  //     }
  //   },
  //   getHistoryBy: ({identifier, comment, meta, data}) => {
  //     return {
  //       'identifier': identifier,
  //       'comment': comment,
  //       'data.INV_ORG_CODE': data.INV_ORG_CODE,
  //       'meta.csvname': {
  //         '$ne': meta.csvname
  //       }
  //     }
  //   }
  // },
  'XX_CEGID_MAIN_WH_QTY_VIEW': {
    'table': 'XX_WINCASH_INT.XX_CEGID_MAIN_WH_QTY_VIEW',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta}) => {
      return {
        'route': 'oracle_cegid', 
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': meta.childGroup
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'oracle_cegid', 
        'identifier': identifier,
        'comment': comment,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'updateStatusBy': ({ITEM_CODE}) => {
      return {
        'identifier': {$in: !Array.isArray(ITEM_CODE) ? [ITEM_CODE] : ITEM_CODE}
      }
    },
    'fieldsToUpdateStatus': ['ITEM_CODE'],
    'filters': [
      {label: 'Item code', field: 'ITEM_CODE'}
    ],
    'toArchiveQuery': ({ identifier }) => {
      return {
        'comment': 'inventory', 
        'identifier': identifier, 
        'meta.childGroup': 'inventory'
      }
    },
    'archiveFrom': 20
  },
  'XX_WINCASH_STAGE_ORDERS_V': {
    'table': 'XX_WINCASH_INT.XX_WINCASH_STAGE_ORDERS_V_TEMP',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup',
        'store': '$data.INV_ORG_CODE'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup', 
        'store': '$data.INV_ORG_CODE', 
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta, data}) => {
      return {
        'route': 'oracle_cegid', 
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': meta.childGroup,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'oracle_cegid', 
        'identifier': identifier,
        'comment': comment,
        'data.INV_ORG_CODE': data.INV_ORG_CODE,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'updateStatusBy': ({ITEM_CODE, INV_ORG_CODE, TRANSFER_ORG, SHIPMENT_NUM, ORDER_NUMBER}) => {
      // Convert item code to integer
      let ITEM_CODE_ARR = !Array.isArray(ITEM_CODE) ? [ITEM_CODE] : ITEM_CODE;
      ITEM_CODE_ARR = ITEM_CODE_ARR.map(str => {
        x = isNumber(str) ? parseInt(str) : str;
        return x;
      });

      const updates = {
        'data.INV_ORG_CODE': {$in: !Array.isArray(INV_ORG_CODE) ? [INV_ORG_CODE] : INV_ORG_CODE},
        'data.ITEM_CODE': {$in: !Array.isArray(ITEM_CODE_ARR) ? [ITEM_CODE_ARR] : ITEM_CODE_ARR}
      };

      if(TRANSFER_ORG) {
        updates['data.TRANSFER_ORG'] = {$in: !Array.isArray(TRANSFER_ORG) ? [TRANSFER_ORG] : TRANSFER_ORG};
      }

      if(SHIPMENT_NUM) {
        updates['data.SHIPMENT_NUM'] = {$in: !Array.isArray(SHIPMENT_NUM) ? [SHIPMENT_NUM] : SHIPMENT_NUM};
      }

      if(ORDER_NUMBER) {
        updates['data.ORDER_NUMBER'] = {$in: !Array.isArray(ORDER_NUMBER) ? [ORDER_NUMBER] : ORDER_NUMBER};
      }
      
      return updates;
    },
    'fieldsToUpdateStatus': ['ITEM_CODE', 'INV_ORG_CODE', 'TRANSFER_ORG', 'SHIPMENT_NUM', 'ORDER_NUMBER'],
    'dateFields': ['ORDER_DATE'],
    'filters': [
      {label: 'Oracle WH code', field: 'INV_ORG_CODE'},
      {label: 'Order no.', field: 'ORDER_NUMBER'},
      {label: 'Item code', field: 'ITEM_CODE'},
      {label: 'Serial no.', field: 'SERIAL_NUMBER'},
      {label: 'Transfer Org.', field: 'TRANSFER_ORG'},
      {label: 'Shipment no.', field: 'SHIPMENT_NUM'}
    ]
  },
  'XX_WINCASH_SALESPERSONS_V': {
    table: 'XX_WINCASH_INT.XX_WINCASH_SALESPERSONS_V',
    'groupBy': {
      '_id': {
        'route': '$route',
        'comment': '$comment',
        'identifier': '$identifier',
        'childGroup': '$meta.childGroup'
      }
    },
    'groupHistoryBy': {
      '_id': {
        'route': '$route', 
        'comment': '$comment', 
        'identifier': '$identifier', 
        'childGroup': '$meta.childGroup',
        'csvname': '$meta.csvname'
      }
    },
    'getHeaderBy': ({identifier, comment, meta}) => {
      return {
        'route': 'oracle_cegid', 
        'comment': comment, 
        'identifier': identifier, 
        'meta.childGroup': meta.childGroup
      }
    },
    'getHistoryBy': ({identifier, comment, meta, data, createdAt}) => {
      return {
        'route': 'oracle_cegid', 
        'identifier': identifier,
        'comment': comment,
        'meta.childGroup': meta.childGroup,
        'meta.csvname': {
          '$ne': meta.csvname
        },
        'createdAt': {
          '$lt': createdAt
        },
      }
    },
    'updateStatusBy': ({OPERATING_UNIT_CODE, SALESREP_NUMBER}) => {
      return {
        'data.SALESREP_NUMBER': {$in: !Array.isArray(SALESREP_NUMBER) ? [SALESREP_NUMBER] : SALESREP_NUMBER},
        'data.OPERATING_UNIT_CODE': {$in: !Array.isArray(OPERATING_UNIT_CODE) ? [OPERATING_UNIT_CODE] : OPERATING_UNIT_CODE}
      }
    },
    'fieldsToUpdateStatus': ['OPERATING_UNIT_CODE', 'SALESREP_NUMBER'],
    'filters': [
      {label: 'Division', field: 'OPERATING_UNIT_CODE'},
      {label: 'Staff no.', field: 'SALESREP_NUMBER'},
      {label: 'Email', field: 'EMAIL'},
      {label: 'Phone no.', field: 'PHONE_NUMBER'}
    ],
    'toArchiveQuery': ({ identifier }) => {
      return {
        'comment': 'staff', 
        'identifier': identifier, 
        'meta.childGroup': 'staff'
      }
    },
    'archiveFrom': 50
  },
  'XX_CEGID_CUSTOMERS_BALANCE': {
    'table': 'XX_WINCASH_INT.XX_CEGID_CUSTOMERS_BALANCE',
    'fieldsToUpdateStatus': [],
    'filters': [
      {label: 'Customer Site No.', field: 'CUSTOMER_SITE_NUMBER'}
    ]
  },
}