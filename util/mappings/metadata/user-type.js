const list = {
  'user': {
    'label': 'Business user',
    'userToCreate': []
  },
  'admin': {
    'label': 'Administrator',
    'userToCreate': ['user', 'it']
  },
  'it': {
    'label': 'I.T',
    'userToCreate': []
  },
  'altavant_support': {
    'label': 'Altavant support',
    'userToCreate': ['altavant_support', 'admin', 'user', 'it']
  },
  'webmaster': {
    'label': 'Developer',
    'userToCreate': ['altavant_support', 'admin', 'user', 'it']
  }
};

module.exports = {
  list: list,
  getMeta: (users, fields = {}) => {
   users = users.map(userkey => {
      const user = list[userkey];
      const userData = {key: userkey};
      for (const field in fields) {
        userData[field] = user[field];
      }
      return userData;
    });

    return users;
  }
}