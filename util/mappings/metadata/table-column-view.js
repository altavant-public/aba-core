const getData = (data, column) => {
  if((typeof data == 'object') && typeof column in data) {
    return data[column];
  }
  return column;
};

const getStoreDetails = (oracleStore) => {
  const STORE_LIST = []; // TODO: This is just a placeholder. New store master list is in /app/mockdata/store-master.json
  const payload = STORE_LIST.filter(x => x.oracle_store_code == oracleStore);

  if(payload.length) {
    let store = payload[0];
    return `${store.description} (${oracleStore}) (${store.cegid_store_code})`
  }
  return oracleStore;
}

module.exports = {
  'XX_WINCASH_INVENTORY_VIEW': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'HEADER_ID', label: 'Document no.'},
      {column: 'INV_ORG_CODE', label: 'Store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      {column: 'CREATION_DATE', label: 'Cegid Creation Date'},
      {label: 'Type', customValue: ({meta}) => {
        const value = meta.childGroup;
        if(value == 'stock_adjustment') {
          return ('Stock Adjustment')
        } else if(value.includes('special_input')) {
          return ('Special Input')
        } else if(value.includes('special_output')) {
          return ('Special Output')
        } else {
          return value;
        }
      }}
    ]
  },
  'XX_WINCASH_STAGE_INVENTORY_V': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'DOC_NUMBER', label: 'Document no.'},
      {column: 'INV_ORG_CODE', label: 'Sender store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      {column: 'DEST_INV_ORG_CODE', label: 'Recipient store', customValue: ({data}) => {
        return getStoreDetails(data.DEST_INV_ORG_CODE);
      }},
      {column: 'CREATION_DATE', label: 'Cegid Creation Date'},
      {column: 'TRANSFER_REASON', label: 'Type'}
    ]
  },
  'XX_WINCASH_STAGE_DEPOSIT_V': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'HEADER_ID', label: 'Receipt no.'},
      {column: 'INV_ORG_CODE', label: 'Store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      {column: 'CREATION_DATE', label: 'Cegid Creation Date'}
    ]
  },
  'XX_WINCASH_STAGE_SALES_V': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'HEADER_ID', label: 'Receipt no.'},
      {column: 'INV_ORG_CODE', label: 'Store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      {column: 'CREATION_DATE', label: 'Cegid Creation Date'},
      {label: 'Type', customValue: ({meta}) => {
        const value = meta.childGroup;
        if(value == 'sale') {
          return ('Sale')
        } else if(value.includes('exchange')) {
          return ('Exchange')
        } else if(value.includes('return')) {
          return ('Return')
        } else if(value.includes('voucher')) {
          return ('Voucher')
        } else {
          return value;
        }
      }}
    ]
  },
  'XX_WINCASH_ITEMS_VIEW': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'ITEM_CODE', label: 'Item code'},
      {column: 'LEGACY_ITEM_CODE', label: 'Legacy item code'},
      {column: 'INV_ORG_CODE', label: 'Store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      // {label: 'Type', customValue: ({data}) => {
      //   const value = data.Identifier;
      //   if(value.includes('ARDC')) {
      //     return ('Item creation')
      //   } else if(value.includes('PCRC')) {
      //     return ('Price')
      //   } else if(value.includes('GATC')) {
      //     return ('Referencing')
      //   } else if(value.includes('TRDC')) {
      //     return ('Item Trad')
      //   } else {
      //     return value;
      //   }
      // }}
    ]
  },
  'XX_WINCASH_CUSTOMERS_VIEW': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'CUSTOMER_NUMBER', label: 'Customer no.'},
      {column: 'CUSTOMER_NAME', label: 'Customer name'}
    ]
  },
  'XX_CEGID_MAIN_WH_QTY_VIEW': {
    getData,
    columns: [
      {column: 'ITEM_CODE', label: 'Item code'}
    ]
  },
  'XX_WINCASH_STAGE_ORDERS_V': {
    getData,
    columns: [
      {column: 'SHIPMENT_NUM', label: 'Shipment no.'},
      {column: 'ORDER_NUMBER', label: 'Order no.'},
      {column: 'INV_ORG_CODE', label: 'Store', customValue: ({data}) => {
        return getStoreDetails(data.INV_ORG_CODE);
      }},
      {label: 'Type', customValue: ({meta}) => {
        const value = meta.childGroup;
        if(value == 'purchase_order') {
          return ('Purchase Order')
        } else if(value.includes('trv_order')) {
          return ('Central WH to Store transfer')
        } else {
          return value;
        }
      }}
    ]
  },
  'XX_WINCASH_SALESPERSONS_V': {
    getData,
    columns: [
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
      {column: 'SALESREP_NUMBER', label: 'Staff no.'},
      {column: 'SALESREP_NAME', label: 'Staff name'},
      {column: 'OPERATING_UNIT_CODE', label: 'Division'},
    ]
  }
}