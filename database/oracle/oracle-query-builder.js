const _HELPERS = {
  generateBinds: (data, fields) => {
    const binds = {};
  
    for (const field of fields) {
      binds[field] = data[field]
    }
  
    return binds;
  },
  whereFields: (conditions) => {
    let whereStr = '';
    const length = conditions.length;
    for (let i = 0; i < length; i++) {
      const condition = conditions[i];
      
      if(i === 0) {
        whereStr = `WHERE ${condition}`;
      } else {
        whereStr = whereStr.concat(` AND ${condition}`);
      }
    }
    return whereStr;
  }
};

const _QUERIES = {
  exist: (table, fields) => {
    let whereStr = '';
    for (const [i, field] of fields.entries()) {
      if(i === 0) {
        whereStr = `WHERE ${field} = :${field}`;
      } else {
        whereStr = whereStr.concat(` AND ${field} = :${field}`);
      }
    }


    return `SELECT ${fields.toString()} FROM ${table} ${whereStr}`;
  }
}

const _COMMANDS = {
  exist: (table, fields, data) => {
    const query = _QUERIES.exist(table, fields);
    const binds = _HELPERS.generateBinds(data, fields);

    return {query, binds};
  }
}

module.exports = {
  commands: _COMMANDS,
  helpers: _HELPERS
}