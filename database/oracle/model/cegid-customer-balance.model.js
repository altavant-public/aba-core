'use strict'

const TABLE_NAME = 'XX_WINCASH_INT.XX_CEGID_CUSTOMERS_BALANCE';
const OracleCore = require('../oracle-core');
const oracleQueryBuilder = require('../oracle-query-builder');
const InterfaceHelper = require('../interface-helpers');

class CegidCustomerBalanceModel extends OracleCore {

  constructor() {
    super('ORACLE', TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }

  filters({ CUSTOMER_SITE_NUMBER,	CREDIT_BALANCE }) {
    this.query = '';
    const conditions = [];

    if(CUSTOMER_SITE_NUMBER) {
      conditions.push(`CUSTOMER_SITE_NUMBER IN (${this.splitStrByComma(CUSTOMER_SITE_NUMBER)})`);
    }
  
    // if(INV_ORG_CODE) {
    //   conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    // }

    // if(ITEM_CODE) {
    //   conditions.push(`ITEM_CODE = '${ITEM_CODE}'`);
    // }

    // if(SERIAL_NUMBER) {
    //   conditions.push(`SERIAL_NUMBER = '${SERIAL_NUMBER}'`);
    // }

    // if(TRANSFER_ORG) {
    //   conditions.push(`TRANSFER_ORG = '${TRANSFER_ORG}'`);
    // }

    // if(DATE_FROM) {
    //   conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    // }

    // if(DATE_TO) {
    //   conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    // }
  
    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

}

module.exports = { CegidCustomerBalanceModel };  