
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_STAGE_DEPOSIT_V';
const oracleQueryBuilder = require('../oracle-query-builder');

class DepositModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }
  
  filters({OPERATING_UNIT_CODE, INV_ORG_CODE, HEADER_ID, JOURNAL_ID, RECEIPT_NUM, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    if(HEADER_ID) {
      conditions.push(`HEADER_ID IN (${this.splitStrByComma(HEADER_ID)})`);
    }
    
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }
    
    if(OPERATING_UNIT_CODE) {
      conditions.push(`OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`);
    }
    
    if(JOURNAL_ID) {
      conditions.push(`JOURNAL_ID = '${JOURNAL_ID}'`);
    }
    
    if(RECEIPT_NUM) {
      conditions.push(`RECEIPT_NUM = '${RECEIPT_NUM}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }

    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }
}

module.exports = DepositModel;  