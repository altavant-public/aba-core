
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_INVENTORY_VIEW';

class DepositModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }
  
  filters({OPERATING_UNIT_CODE, INV_ORG_CODE, HEADER_ID, ITEM_CODE, SERIAL_NUMBER}) {
    this.query = '';

    if(HEADER_ID) {
      this.query = `WHERE HEADER_ID IN (${this.splitStrByComma(HEADER_ID)})`;
    }
  
    if(INV_ORG_CODE) {
      this.query = `${this.query} AND INV_ORG_CODE = '${INV_ORG_CODE}'`;
    }

    if(OPERATING_UNIT_CODE) {
      this.query = `${this.query} AND OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`;
    }

    if(ITEM_CODE) {
      this.query = `${this.query} AND ITEM_CODE = '${ITEM_CODE}'`;
    }

    if(SERIAL_NUMBER) {
      this.query = `${this.query} AND SERIAL_NUMBER = '${SERIAL_NUMBER}'`;
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }
}

module.exports = DepositModel;  