
const OracleCore = require('../oracle-core');
const { CSVBuilderService } = require('@services');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_ITEMS_VIEW_TEMP';
// const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_ITEMS_VIEW';
const InterfaceHelper = require('../interface-helpers');
const oracleQueryBuilder = require('../oracle-query-builder');

class ItemModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }

  filters({OPERATING_UNIT_CODE, INV_ORG_CODE, ITEM_CODE, LEGACY_ITEM_CODE, ITEM_TYPE, REC_STATUS, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    if(ITEM_CODE) {
      conditions.push(`ITEM_CODE IN (${this.splitStrByComma(ITEM_CODE)})`);
    }

    if(LEGACY_ITEM_CODE) {
      conditions.push(`LEGACY_ITEM_CODE IN (${this.splitStrByComma(LEGACY_ITEM_CODE)})`);
    }
  
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }

    if(OPERATING_UNIT_CODE) {
      conditions.push(`OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`);
    }

    if(ITEM_TYPE) {
      conditions.push(`ITEM_TYPE = '${ITEM_TYPE}'`);
    }

    if(REC_STATUS !== undefined) {
      conditions.push(`REC_STATUS = '${REC_STATUS}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }
    
    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
    
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

  async exportAsInterface(rows) {
    const links = [];
    const dateStr = Date.now();
    const files = [
      {filename: `ORACLE_ITEM_${dateStr}.csv`, identifier: 'ARDC1x'},
      {filename: `ORACLE_ITEM_PRICE_${dateStr}.csv`, identifier: 'PCRC1x'},
      {filename: `ORACLE_ITEM_REFERENCING_${dateStr}.csv`, identifier: 'GATC1x'}
    ];

    for (const item of files) {
      let result = await this.generateCSV(item, rows);
      links.push(result);
    }

    return {link: links};
  }

  async generateCSV({filename, identifier}, rows) {
    try {
      const csv = new CSVBuilderService(filename, process.env.APP_TEMP_EXPORT_FILE);
      const helper = InterfaceHelper[this.table];
      const result = await csv.rows(helper.generateCSVRow(rows, identifier)).setColumn(helper.getCSVFileHeader()).generate();
      const link = `${process.env.APP_URL}${process.env.APP_TEMP_EXPORT_URL_PATH}/${filename}`;
      return {link: link, error: result.error};
    } catch (error) {
      return {link: 'n/a', error: error};
    }
  }

}

module.exports = ItemModel;  