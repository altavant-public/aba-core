
const OracleCore = require('../oracle-core');
const oracleQueryBuilder = require('../oracle-query-builder');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_STAGE_SALES_V';
const CREATE_ITEM_QUERY =
 `insert into ${TABLE_NAME} (
     OPERATING_UNIT_NAME,
     OPERATING_UNIT_CODE,
     INV_ORG_CODE,
     HEADER_ID,
     REC_TYPE,
     TRANSACTION_DATE,
     SALESMAN_CODE,
     SALESMAN_NAME,
     CUSTOMER_NUMBER,
     SITE_NAME,
     CUST_SITE_NUM,
     CUSTOMER_NAME,
     ADDRESS,
     MOBILE_NUM,
     EMAIL,
     INVOICE_AMOUNT,
     CURRENCY_CODE,
     EXCHANGE_RATE,
     CURRENCY_DATE,
     TERMINAL_NUMBER,
     PAY_METHOD,
     PAY_AMOUNT,
     DESCRIPTION,
     LOCATION_CODE,
     BANK_NAME,
     CHECK_NUMBER,
     CHECK_DATE,
     LINE_NUMBER,
     ITEM_CODE,
     ITEM_UOM,
     ITEM_QTY,
     ITEM_PRICE,
     DISCOUNT_AMT,
     SERIAL_NUMBER,
     EXPIRY_DATE,
     CREATED_BY,
     CREATION_DATE,
     MODIFIED_DATE,
     MODIFIED_BY,
     REC_STATUS,
     ERROR_MSG,
     BANK_ACCOUNT_NUM,
     SEQ_HEADER_ID,
     CASH_CUSTOMER_REF,
     CIVIL_ID,
     TAX_CLASSIFICATION,
     VAT_AMOUNT,
     TAX_NAME
  ) values (
     :OPERATING_UNIT_NAME,
     :OPERATING_UNIT_CODE,
     :INV_ORG_CODE,
     :HEADER_ID,
     :REC_TYPE,
     :TRANSACTION_DATE,
     :SALESMAN_CODE,
     :SALESMAN_NAME,
     :CUSTOMER_NUMBER,
     :SITE_NAME,
     :CUST_SITE_NUM,
     :CUSTOMER_NAME,
     :ADDRESS,
     :MOBILE_NUM,
     :EMAIL,
     :INVOICE_AMOUNT,
     :CURRENCY_CODE,
     :EXCHANGE_RATE,
     :CURRENCY_DATE,
     :TERMINAL_NUMBER,
     :PAY_METHOD,
     :PAY_AMOUNT,
     :DESCRIPTION,
     :LOCATION_CODE,
     :BANK_NAME,
     :CHECK_NUMBER,
     :CHECK_DATE,
     :LINE_NUMBER,
     :ITEM_CODE,
     :ITEM_UOM,
     :ITEM_QTY,
     :ITEM_PRICE,
     :DISCOUNT_AMT,
     :SERIAL_NUMBER,
     :EXPIRY_DATE,
     :CREATED_BY,
     :CREATION_DATE,
     :MODIFIED_DATE,
     :MODIFIED_BY,
     :REC_STATUS,
     :ERROR_MSG,
     :BANK_ACCOUNT_NUM,
     :SEQ_HEADER_ID,
     :CASH_CUSTOMER_REF,
     :CIVIL_ID,
     :TAX_CLASSIFICATION,
     :VAT_AMOUNT,
     :TAX_NAME
  )`;

class StageSaleModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }

  filters({INV_ORG_CODE, HEADER_ID, OPERATING_UNIT_CODE, ITEM_CODE, SERIAL_NUMBER, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    if(HEADER_ID) {
      conditions.push(`HEADER_ID IN (${this.splitStrByComma(HEADER_ID)})`);
    }
    
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }

    if(OPERATING_UNIT_CODE) {
      conditions.push(`OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`);
    }

    if(ITEM_CODE) {
      conditions.push(`ITEM_CODE = '${ITEM_CODE}'`);
    }

    if(SERIAL_NUMBER) {
      conditions.push(`SERIAL_NUMBER = '${SERIAL_NUMBER}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }

    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

  async storeData(item) {
    const result = await this.execute(CREATE_ITEM_QUERY, item, { autoCommit: true});
    return result;
  }
}

module.exports = StageSaleModel;  