
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_SALESPERSONS_V';
const INTERFACE_CSV_FILENAME = 'ORACLE_SALESPERSON.csv';

class StaffModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME, {INTERFACE_CSV_FILENAME});
    this.query = '';
    this.limitCount = '';
  }

  filters({OPERATING_UNIT_CODE, SALESREP_NUMBER, EMAIL, PHONE_NUMBER}) {
    this.query = '';

    if(SALESREP_NUMBER) {
      this.query = `WHERE SALESREP_NUMBER IN (${this.splitStrByComma(SALESREP_NUMBER)})`;
    }
  
    if(OPERATING_UNIT_CODE) {
      this.query = `${this.query} AND OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`;
    }

    if(EMAIL) {
      this.query = `${this.query} AND EMAIL = '${EMAIL}'`;
    }

    if(PHONE_NUMBER) {
      this.query = `${this.query} AND PHONE_NUMBER = '${PHONE_NUMBER}'`;
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

}

module.exports = StaffModel;  