
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_CEGID_MAIN_WH_QTY_VIEW';
const INTERFACE_CSV_FILENAME = 'ORACLE_INV.csv';

class InventoryModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME, {INTERFACE_CSV_FILENAME});
    this.query = '';
    this.limitCount = '';
  }

  filters({ITEM_CODE}) {
    this.query = '';

    if(ITEM_CODE) {
      this.query = `WHERE ITEM_CODE IN (${this.splitStrByComma(ITEM_CODE)})`;
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

}

module.exports = InventoryModel;  