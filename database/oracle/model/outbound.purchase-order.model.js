
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_STAGE_ORDERS_V_TEMP';
const INTERFACE_CSV_FILENAME = 'ORACLE_PO.csv';
const oracleQueryBuilder = require('../oracle-query-builder');
const InterfaceHelper = require('./../interface-helpers');
const INTERFACE_HELPER = InterfaceHelper[TABLE_NAME]['PO'];

class PurchaseOrderModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME, {INTERFACE_CSV_FILENAME, INTERFACE_HELPER});
    this.query = '';
    this.limitCount = '';
  }

  filters({INV_ORG_CODE, ORDER_NUMBER, ITEM_CODE, SERIAL_NUMBER, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    conditions.push('REC_TYPE = 201');

    if(ORDER_NUMBER) {
      conditions.push(`ORDER_NUMBER IN (${this.splitStrByComma(ORDER_NUMBER)})`);
    }
  
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }

    if(ITEM_CODE) {
      conditions.push(`ITEM_CODE = '${ITEM_CODE}'`);
    }

    if(SERIAL_NUMBER) {
      conditions.push(`SERIAL_NUMBER = '${SERIAL_NUMBER}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }
  
    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  headerExistFilter({INV_ORG_CODE, ORDER_NUMBER}) {
    this.query = `WHERE REC_TYPE = 201 AND INV_ORG_CODE = '${INV_ORG_CODE}' AND ORDER_NUMBER = '${ORDER_NUMBER}'`;
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

}

module.exports = PurchaseOrderModel;  