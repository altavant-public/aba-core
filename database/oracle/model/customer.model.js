
const OracleCore = require('../oracle-core');
const { CSVBuilderService } = require('@services');
const InterfaceHelper = require('../interface-helpers')
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_CUSTOMERS_VIEW_TEMP';
const INTERFACE_CSV_FILENAME = 'ORACLE_CUSTOMERS.csv';

class CustomerModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME, {INTERFACE_CSV_FILENAME});
    this.query = '';
    this.limitCount = '';
  }

  filters({OPERATING_UNIT_CODE, CUSTOMER_NUMBER}) {
    this.query = '';

    if(CUSTOMER_NUMBER) {
      this.query = `WHERE CUSTOMER_NUMBER IN (${this.splitStrByComma(CUSTOMER_NUMBER)})`;
    }

    if(OPERATING_UNIT_CODE) {
      this.query = `${this.query} AND OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`;
    }
    
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }
}

module.exports = CustomerModel;  