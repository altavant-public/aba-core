
const OracleCore = require('../oracle-core');
const oracleQueryBuilder = require('../oracle-query-builder');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_STAGE_INVENTORY_V';

class TransferModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME);
    this.query = '';
    this.limitCount = '';
  }
  
  filters({OPERATING_UNIT_CODE, INV_ORG_CODE, DOC_NUMBER, ITEM_CODE, SERIAL_NUMBER, DEST_INV_ORG_CODE, TRANSFER_REASON, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    if(DOC_NUMBER) {
      conditions.push(`DOC_NUMBER IN (${this.splitStrByComma(DOC_NUMBER)})`);
    }
    
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }
    
    if(OPERATING_UNIT_CODE) {
      conditions.push(`OPERATING_UNIT_CODE = '${OPERATING_UNIT_CODE}'`);
    }
    
    if(ITEM_CODE) {
      conditions.push(`ITEM_CODE = '${ITEM_CODE}'`);
    }
    
    if(SERIAL_NUMBER) {
      conditions.push(`SERIAL_NUMBER = '${SERIAL_NUMBER}'`);
    }
    
    if(DEST_INV_ORG_CODE) {
      conditions.push(`DEST_INV_ORG_CODE = '${DEST_INV_ORG_CODE}'`);
    }
    
    if(TRANSFER_REASON) {
      conditions.push(`TRANSFER_REASON = '${TRANSFER_REASON}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }

    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }
}

module.exports = TransferModel;  