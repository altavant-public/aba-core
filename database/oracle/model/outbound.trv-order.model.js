
const OracleCore = require('../oracle-core');
const TABLE_NAME = 'XX_WINCASH_INT.XX_WINCASH_STAGE_ORDERS_V_TEMP';
const INTERFACE_CSV_FILENAME = 'ORACLE_TRV.csv';
const oracleQueryBuilder = require('../oracle-query-builder');
const InterfaceHelper = require('./../interface-helpers');
const INTERFACE_HELPER = InterfaceHelper[TABLE_NAME]['TRV'];

class TRVOrderModel extends OracleCore {

  constructor(database) {
    super(database, TABLE_NAME, {INTERFACE_CSV_FILENAME, INTERFACE_HELPER});
    this.query = '';
    this.limitCount = '';
  }

  filters({INV_ORG_CODE, TRANSFER_ORG, SHIPMENT_NUM, ITEM_CODE, SERIAL_NUMBER, DATE_FIELD, DATE_FROM, DATE_TO}) {
    this.query = '';
    const conditions = [];

    conditions.push('REC_TYPE = 202');

    if(SHIPMENT_NUM) {
      conditions.push(`SHIPMENT_NUM IN (${this.splitStrByComma(SHIPMENT_NUM)})`);
    }
  
    if(INV_ORG_CODE) {
      conditions.push(`INV_ORG_CODE = '${INV_ORG_CODE}'`);
    }

    if(ITEM_CODE) {
      conditions.push(`ITEM_CODE = '${ITEM_CODE}'`);
    }

    if(SERIAL_NUMBER) {
      conditions.push(`SERIAL_NUMBER = '${SERIAL_NUMBER}'`);
    }

    if(TRANSFER_ORG) {
      conditions.push(`TRANSFER_ORG = '${TRANSFER_ORG}'`);
    }

    if(DATE_FROM) {
      conditions.push(`${DATE_FIELD} >= ${this.formatDate(DATE_FROM)}`);
    }

    if(DATE_TO) {
      conditions.push(`${DATE_FIELD} <= ${this.formatDate(DATE_TO)}`);
    }
  
    if(conditions.length) {
      this.query = oracleQueryBuilder.helpers.whereFields(conditions);
    }
  
    return this;
  }

  headerExistFilter({INV_ORG_CODE, SHIPMENT_NUM}) {
    this.query = `WHERE REC_TYPE = 202 AND INV_ORG_CODE = '${INV_ORG_CODE}' AND SHIPMENT_NUM = '${SHIPMENT_NUM}'`;
    return this;
  }

  limit(limit) {
    this.limitCount = `FETCH NEXT ${limit} ROWS ONLY`;
    return this;
  }

}

module.exports = TRVOrderModel;  