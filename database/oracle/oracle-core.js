const oracledb = require('oracledb');
const path = require('path');
const config = require('./oracle-config');
const { CSVBuilderService } = require('@services');
const { Logger } = require('@helpers');
const InterfaceHelper = require('./interface-helpers')
const OracleQueryBuilder = require('./oracle-query-builder');
const { UtilHelper } = require('@helpers');

class OracleCore {
  
  constructor(database, table, options = {}) {
    this.database = database;
    this.table = table;
    this.config = this.getConfig();
    this.connection;
    this.options = options;
  }

  /**
   * 
   * @param {*} statement 
   * @param {*} binds 
   * @param {*} opts 
   * @returns Object - { error: String, rows: Array[], metaData: Array[] }
   */
  async execute(statement, binds = [], opts = {}) {
    opts.outFormat = oracledb.OBJECT;
  
    try {
      this.connection = await oracledb.getConnection(this.config);
      const result = await this.connection.execute(statement, binds, opts);
      // const { result } = require('@root/sample-customer-balance-success.json'); // For debugging purposes only
      return responseParser(result);
    } catch (err) {
      Logger.oracle("database connection error \n" + statement + "\n" + JSON.stringify(err.message));
      return errorParser(err);
    } finally {
      if (this.connection) {
        try {
          await this.connection.close();
        } catch (err) {
          Logger.oracle("database connection finally error \n" + JSON.stringify(err.message));
        }
      }
    }
  }

  getConfig() {
    try {
      return config[this.database];
    } catch (error) {
      Logger.oracle(`Error loading oracle db config for ${this.database}`, error);
      return {};
    }
  }

  splitStrByComma(str) {
    return `'${str.split(',').join("','")}'`;
  }

  async exportAsCSV(rows, {req}) {
    const filename = `${req.path.replace('/', '')}_${Date.now()}`;
    const csv = new CSVBuilderService(filename, process.env.APP_TEMP_EXPORT_FILE);
    const result = await csv.rows(rows).generate();
    const link = `${process.env.APP_URL}${process.env.APP_TEMP_EXPORT_URL_PATH}/${filename}`;
    return {link: link, error: result.error};
  }

  async exportAsInterface(rows) {
    const filename = `${path.parse(this.options.INTERFACE_CSV_FILENAME).name}_${Date.now()}.csv`;
    const csv = new CSVBuilderService(filename, process.env.APP_TEMP_EXPORT_FILE);
    const helper = this.options.INTERFACE_HELPER ? this.options.INTERFACE_HELPER : InterfaceHelper[this.table];
    const result = await csv.rows(helper.generateCSVRow(rows)).setColumn(helper.getCSVFileHeader()).generate();
    const link = `${process.env.APP_URL}${process.env.APP_TEMP_EXPORT_URL_PATH}/${filename}`;
    return {link: link, error: result.error};
  }

  /**
   * 
   * @param {*} result 
   * @param {*} query 
   * @returns Object - { error?: String, rows?: Array[], metaData?: Array[], query: String, link?: String }
   */
  async response(result, query) {
    if(result.error || !this.options.return_type) return { ...result, query};
    let response = {result, query};

    switch (req.query.return_type) {
      case 'csv':
        response = await this.exportAsCSV(result.rows, {req});
        break;

      case 'interface':
        response = await this.exportAsInterface(result.rows);
        break;
    
      default:
        break;
    }

    return response;
  }
  
  /**
   * 
   * @param {Object} req - Express HTTP request
   * @returns Object - { error: String, rows: Array[], metaData: Array[], query: String }
   */
  async run() {
    try {
      const mainQuery = `SELECT * FROM ${this.table}`;
      const query = `${mainQuery} ${this.query} ${this.limitCount}`;
      const result = await this.execute(query);
      const response = await this.response(result, query);
      return response;
    } catch (error) {
      return { error: UtilHelper.getErrorStack(error) };
    }
  }

  async custom(query, args) {
    try {
      const result = await this.execute(query);
      const response = await this.response({query: {return_type: args.return_type}}, result, query);
      return response;
    } catch (error) {
      return error.message;
    }
  }
  
  async exist(data, fields) {
    const { query, binds } = OracleQueryBuilder.commands.exist(this.table, fields, data);
    const result = await this.execute(query, binds);
    
    if(('rows' in result) && result.rows.length > 0) return true;
    return false;
  }

  /**
   * TODO: TEMPORARY FUNCTION ONLY. NEED TO REFACTOR run() to handle all use case.
   * @param {Object} args - Custom arguments
   * @returns 
   */
  async runTemp(args = {}) {
    try {
      const mainQuery = `SELECT * FROM ${this.table}`;
      const query = `${mainQuery} ${this.query} ${this.limitCount}`;
      const result = await this.execute(query);
      return result;
    } catch (error) {
      return error.message;
    }
  }

  formatDate(date) {
    const moment = require('moment');
    const formattedDate = moment.utc(date).format('YYYY-MM-DD HH:mm:ss');
    return `TO_DATE('${formattedDate}', 'yyyy-mm-dd hh24:mi:ss')`;
  }
}

/**
 * 
 * @param {Object} errorObject 
 * @returns Object - { error: String }
 */
const errorParser = (errorObject) => {
  let error = '';
  try {
    if('message' in errorObject) {
      error = errorObject.message;
    } else {
      error = JSON.stringify(error);
    }
  } catch (_) {
    error = errorObject
  }

  return { error };
}

/**
 * 
 * @param {Object} result 
 * @returns Object - { error: String, rows: Array[], metaData: Array[] }
 */
const responseParser = (result) => {
  try {
    if('rows' in result) {
      return { ...result };
    } else {
      throw new Error('Unable to retrieve rows information');
    }
  } catch (_) {
    return { error: error.message };
  }
}

module.exports = OracleCore;