module.exports = {
  ORACLE: {
    user          : process.env.ORACLE_USERNAME,
    password      : process.env.ORACLE_PW,
    connectString : process.env.ORACLE_HOST,
    poolMin: 10,
    poolMax: 10,
    poolIncrement: 1,
    poolPingInterval: 60,
    poolTimeout: 60
  }
}