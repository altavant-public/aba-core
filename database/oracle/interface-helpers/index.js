module.exports = {
  'XX_WINCASH_INT.XX_WINCASH_CUSTOMERS_VIEW_TEMP': require('./helpers/customer.interface-helper'),
  'XX_WINCASH_INT.XX_CEGID_MAIN_WH_QTY_VIEW': require('./helpers/inventory.interface-helper'),
  'XX_WINCASH_INT.XX_WINCASH_ITEMS_VIEW_TEMP': require('./helpers/item.interface-helper'),
  'XX_WINCASH_INT.XX_WINCASH_SALESPERSONS_V': require('./helpers/staff.interface-helper'),
  'XX_WINCASH_INT.XX_WINCASH_STAGE_ORDERS_V_TEMP': require('./helpers/stage-order.interface-helper')
}