const getCSVFileHeader = (string) => {
  return [
    { id: 'identifier', title: 'Identifier' },
    { id: 'item_code', title: 'ITEM_CODE' },
    { id: 'on_hand_qty', title: 'ON_HAND_QTY' }
  ];
};

const generateCSVRow = (rows) => {
  return rows.map(data => {
    return {
      identifier: 'INVC1x',
      item_code: data.ITEM_CODE,
      on_hand_qty: data.ON_HAND_QTY,
    }
  });
};

module.exports = {getCSVFileHeader, generateCSVRow};