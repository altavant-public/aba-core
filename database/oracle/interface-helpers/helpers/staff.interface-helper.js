const getCSVFileHeader = (string) => {
  return [
    { id: 'identifier', title: 'Identifier' },
    { id: 'operating_unit_code', title: 'OPERATING_UNIT_CODE' },
    { id: 'salesrep_name', title: 'SALESREP_NAME' },
    { id: 'salesrep_number', title: 'SALESREP_NUMBER' },
    { id: 'wincash_salesrep_ref', title: 'WINCASH_SALESREP_REF' },
    { id: 'status', title: 'STATUS' },
    { id: 'phone_number', title: 'PHONE_NUMBER' },
    { id: 'email', title: 'EMAIL' },
  ];
};

const generateCSVRow = (rows) => {
  return rows.map(data => {
    return {
      identifier: 'COMC1x',
      operating_unit_code: data.OPERATING_UNIT_CODE,
      salesrep_name: data.SALESREP_NAME,
      salesrep_number: data.SALESREP_NUMBER,
      wincash_salesrep_ref: data.WINCASH_SALESREP_REF,
      status: data.STATUS,
      phone_number: data.PHONE_NUMBER,
      email: data.EMAIL
    };
  });
};

module.exports = {getCSVFileHeader, generateCSVRow};