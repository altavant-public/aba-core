const moment = require('moment');

const PO = {
  getCSVFileHeader: (string) => {
    return [
      { id: 'identifier', title: 'Identifier' },
      { id: 'inv_org_code', title: 'INV_ORG_CODE' },
      { id: 'order_number', title: 'ORDER_NUMBER' },
      { id: 'order_date', title: 'ORDER_DATE' },
      { id: 'item_code', title: 'ITEM_CODE' },
      { id: 'qty_rec', title: 'QTY_REC' },
      { id: 'unit_price', title: 'UNIT_PRICE' },
      { id: 'serial_number', title: 'SERIAL_NUMBER' },
    ];
  },
  generateCSVRow: (rows) => {
    return rows.map(data => {
      return {
        identifier: 'ALFC1x',
        inv_org_code: data.INV_ORG_CODE,
        order_number: `${data.INV_ORG_CODE}-${data.ORDER_NUMBER}-${data.SHIPMENT_NUM}`,
        order_date: moment(data.ORDER_DATE).format('DD/MM/YYYY'),
        item_code: data.ITEM_CODE,
        qty_rec: data.QTY_REC,
        unit_price: data.UNIT_PRICE,
        serial_number: data.SERIAL_NUMBER,
      };
    });
  }
}

const TRV = {
  getCSVFileHeader: (string) => {
    return [
      { id: 'identifier', title: 'Identifier' },
      { id: 'inv_org_code', title: 'INV_ORG_CODE' },
      { id: 'transfer_org', title: 'TRANSFER_ORG' },
      { id: 'shipment_num', title: 'SHIPMENT_NUM' },
      { id: 'order_date', title: 'ORDER_DATE' },
      { id: 'item_code', title: 'ITEM_CODE' },
      { id: 'qty_rec', title: 'QTY_REC' },
      { id: 'unit_price', title: 'UNIT_PRICE' },
      { id: 'serial_number', title: 'SERIAL_NUMBER' },
    ];
  },
  generateCSVRow: (rows) => {
    return rows.map(data => {
      return {
        identifier: 'TRVC1x',
        inv_org_code: data.INV_ORG_CODE,
        transfer_org: data.TRANSFER_ORG,
        shipment_num: data.SHIPMENT_NUM,
        order_date: moment(data.ORDER_DATE).format('DD/MM/YYYY'),
        item_code: data.ITEM_CODE,
        qty_rec: data.QTY_REC,
        unit_price: data.UNIT_PRICE,
        serial_number: data.SERIAL_NUMBER,
      };
    });
  }
}

module.exports = {PO, TRV};