module.exports = {
  ...require('./cegid-log'),
  ...require('./customer-credit-balance'),
  ...require('./discrepancy-report'),
  ...require('./inbound_header'),
  ...require('./outbound_header'),
  ...require('./outbound-archive'),
  ...require('./outbound-header-archive'),
  ...require('./outbound'),
  ...require('./promotion-log'),
  ...require('./queue'),
  ...require('./repair-confirmation'),
  ...require('./sales-master'),
  ...require('./settings'),
  ...require('./staging-order-header'),
  ...require('./staging-order-lines'),
  ...require('./staging-sales'),
  ...require('./system-error'),
  // ...require('./cegid-transfer-report.model'),
  // ...require('./inbound.model'),
  // ...require('./user.model'),
}