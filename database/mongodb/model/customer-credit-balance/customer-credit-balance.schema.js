const mongoose = require('mongoose');
const { STATUS } = require('./customer-credit-balance.helper');

// Schema
const schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  data: {
    previous_oracle_credit_balance: {type: Number, required: true, default: 0},
    oracle_credit_balance: {type: Number, required: true, default: 0},
    cegid_deposit: {type: Number, required: true, default: 0},
    cegid_credit_note: {type: Number, required: true, default: 0},
    cegid_credit_balance: {type: Number, required: true, default: 0}
  },
  history: [{type: Object}],
  meta: {type: Object, default: {}},
  isInCegid: {type: Boolean},
  status: {
    type: String,
    required: true,
    enums: [STATUS.QUEUED, STATUS.SUCCESS, STATUS.FAILED, STATUS.DONE, STATUS.PROCESSING],
    default: 'queued'
  },
}, {
  timestamps: true
});

// Indexes
schema.index({ id: 1  });

module.exports = schema;