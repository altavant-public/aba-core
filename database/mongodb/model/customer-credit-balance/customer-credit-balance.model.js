const MongoDBModel = require('../../util/mongodb-model');
const { STATUS, IS_VALID_NUMBER } = require('./customer-credit-balance.helper');

class Model extends MongoDBModel {
  constructor() {
    super('customer-credit-balance');
    this.document = {};
  }

  /**
   * 
   * @param {*} customerId, oracleCreditBalance, cegidDeposit, cegidCreditNote, cegidCreditBalance, isInCegid 
   * @returns 
   */
  async createOrUpdate({ customerId, oracleCreditBalance, previousOracleCreditBalance, cegidDeposit, cegidCreditNote, cegidCreditBalance, isInCegid, status, meta }) {
    const document = { id: customerId };

    if(IS_VALID_NUMBER(previousOracleCreditBalance)) {
      document['data.previous_oracle_credit_balance'] = previousOracleCreditBalance;
    }

    if(IS_VALID_NUMBER(oracleCreditBalance)) {
      document['data.oracle_credit_balance'] = oracleCreditBalance;
    }

    if(IS_VALID_NUMBER(cegidDeposit)) {
      document['data.cegid_deposit'] = cegidDeposit;
    }

    if(IS_VALID_NUMBER(cegidCreditNote)) {
      document['data.cegid_credit_note'] = cegidCreditNote;
    }

    if(IS_VALID_NUMBER(cegidCreditBalance)) {
      document['data.cegid_credit_balance'] = cegidCreditBalance;
    }
    
    if(isInCegid !== undefined) {
      document['isInCegid'] = isInCegid;
    }

    document['status'] = status ? status : STATUS.QUEUED;

    // Update meta fields
    const metaFields = meta ? meta : {};
    for (const key in metaFields) {
      document[`meta.${key}`] = metaFields[key];
    }

    const filter = { id: customerId };
    const options = {new: true, upsert: true};
    const result = await this.model.findOneAndUpdate(filter, document, options);

    return result;
  }

  /**
   * Only import if there is no "queued" record
   * @returns Boolean
   */
  async canImportOracleCustomer() {
    try {
      const query = {
        'status': STATUS.QUEUED
      };
  
      const isExist = await this.exists(query);
      return !isExist;
    } catch (_) {
      return true;
    }
  }

  async recordToUpdate(id) {
    try {
      const query = {
        'status': STATUS.QUEUED
      };

      if(id) {
        query['id'] = id;
      }
  
      const result = await this.getOne(query);
      return result;
    } catch (_) {
      return {}
    }
  }

  /**
   * Update record after trying to Cegid. Error or success.
   * @returns 
   */
  async updateOnCegidUpdate(customerId, { error, customerBalance, isInCegid }) {
    try {
      let args = { customerId };

      if(customerBalance) {
        const { oracleCreditBalance, cegidDeposit, cegidCreditNote, cegidCreditBalance, previousOracleCreditBalance } = customerBalance;
        args = { ...args, oracleCreditBalance, cegidDeposit, cegidCreditNote, cegidCreditBalance, previousOracleCreditBalance, isInCegid, status: STATUS.DONE };

        // Build meta values
        if(isInCegid) {
          args['meta'] = { cegidLastUpdate: new Date(), justUpdated: true }
        } else {
          args['meta'] = { justUpdated: false }
        }
      } else {
        args = { ...args, status: STATUS.FAILED, meta: { error } };
      }

      const result = await this.createOrUpdate(args);
      return result;
    } catch (error) {
      return { error };
    }
  }

  /**
   * Check how many transactions has been updated in Cegid
   * @returns Total records
   */
  async updatedRecordsInCegid() {
    try {
      const query = {
        'status': STATUS.DONE,
        'meta.justUpdated': true
      };
  
      const { payload } = await this.countDocuments(query);
      return payload;
    } catch (_) {
      return 0;
    }
  }

  /**
   * Check how many transactions has been skipped to update in Cegid
   * @returns Total records
   */
  async skippedCegidUpdates() {
    try {
      const query = {
        'status': STATUS.DONE,
        'meta.justUpdated': false
      };
  
      const { payload } = await this.countDocuments(query);
      return payload;
    } catch (_) {
      return 0;
    }
  }
}

module.exports = new Model();