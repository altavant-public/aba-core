const helpers = {};

helpers.STATUS = {
  DONE: 'done',
  FAILED: 'failed',
  PROCESSING: 'processing',
  QUEUED: 'queued',
  SUCCESS: 'success'
};

helpers.IS_VALID_NUMBER = (input) => {
  // Use parseFloat to convert the input to a floating-point number
  var number = parseFloat(input);
  
  // Use isNaN to check if the conversion resulted in a valid number
  if (isNaN(number)) {
    // If it's not a valid number, return false or handle the error
    return false;
  }

  // If you want to further process or use the valid number, you can do it here
  return true;
}

module.exports = helpers;