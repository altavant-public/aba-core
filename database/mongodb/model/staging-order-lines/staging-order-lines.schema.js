const mongoose = require('mongoose');

/**
 * Status Guideline
 * 
 * "QUEUED":
 * - Initial status of the record.
 * - Only generate interface data for this status
 * - Don't proceed to interface generation if this status exist upon checking from background task
 * 
 * "LINES_EXPORTED":
 * - Status if lines already exported as an interface file
 */
const schema = mongoose.Schema({
  key: { // The unique number to use in Cegid to handle PO unicity.
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  type: {type: String, enum: ['alf','trv']}, // Transaction type if it's PO or Transfer
  meta: {
    status: {type: String, required: true, default: 'queued', enum: ['queued', 'lines_exported', 'processing', 'failed']},
    error: {type: String},
    oracle_status: {type: String, enum: ['oracle_rec_status_updated', 'lines_exported_with_error']},
    oracle_error: {type: String},
    note: {type: String}
  },
  filters: { type: Object },
  data: [{type: Object}],
  valid: {type: Boolean, default: false}
}, {
  timestamps: true
});

// Indexes
schema.index({ key: 1 });
schema.index({ type: 1 });
// schema.index({ valid: 1 });
// schema.index({ 'meta.totalLines': 1 });
schema.index({ 'meta.status': 1 });
schema.index({ 'filters.OPERATING_UNIT_CODE': 1, 'filters.INV_ORG_CODE': 1, 'filters.ORDER_NUMBER': 1, 'filters.TRANSFER_ORG': 1, 'filters.SHIPMENT_NUM': 1 });
// schema.index({ createdAt: -1 });
// schema.index({ updatedAt: -1 });

/**
 * Set index to autodelete this record
 */
// const secondsInADay = 86400;
const secondsPerMinute = 60;
const duration = (secondsPerMinute * 3.5); // Delete records older than 3.5 minutes
schema.index({ createdAt: 1 }, { expireAfterSeconds: duration, partialFilterExpression: { 'meta.status': { $eq: 'lines_exported' } } });

module.exports = schema;