const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  meta: {type: Object},
  data: [{type: Object}],
}, {
  timestamps: true
});

module.exports = schema;