const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('promotion-log');
    this.document = {};
  }

  async generateDocument(data, meta) {
    const result = await this.save({ data, meta });
    return result;
  }

  generateSummary(records) {
    return records.map(data =>  {
      const summary = {srcLink:'', files: '', id: data._id, ...data};
      const files = Object.values(data.meta?.files || []);

      if(files.length) {
        summary['srcLink'] = `<a href="/api/downloads?path=${process.env.APP_TEMP_EXPORT_FILE}&filename=${files[0]['sourceFile']}" target="_blank">${files[0]['sourceFile']}</a>`;
        
        // Generate files link
        for (const x of files) {
          summary['files'] += `<a href="/api/downloads?path=${process.env.APP_TEMP_EXPORT_FILE}&filename=${x.generatedFile}" target="_blank">${x.generatedFile}</a><br />`;
        }
      }
      
      return summary;
    });
    
  }
}

module.exports = Model;