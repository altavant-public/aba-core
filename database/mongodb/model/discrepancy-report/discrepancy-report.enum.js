const STATUS_MODEL = {
  enum: ['queued', 'processing', 'success', 'failed'],
  default: 'queued'
};

module.exports = {
  status: STATUS_MODEL,
  transactions: {
    status: STATUS_MODEL
  },
  route: ['cegid_oracle', 'oracle_cegid'],
  type: {
    order: {
      value: 'order',
      label: 'Purchase / TRV Orders',
      rowFields: [
        'oracle_store_code',
        'cegid_store_code',
        'cegid_document_number',
        'reference'
      ]
    },
    transfers: {
      value: 'transfers',
      label: 'Transfers',
      rowFields: []
    }
  }
};