const mongoose = require('mongoose');
const enumlist = require('./discrepancy-report.enum');

// Schema
const schema = mongoose.Schema({
  name: { type: String, required: true },
  route: { type: String, enum: enumlist.route, required: true },
  transactions: [{
    status: { type: String, enum: enumlist.transactions.status.enum, default: enumlist.transactions.status.default },
    data: { type: Object }
  }],
  status: {
    type: String,
    required: true,
    enum: enumlist.status.enum,
    default: enumlist.status.default
  },
  type: {
    type: String,
    required: true,
    enum: Object.keys(enumlist.type)
  },
  meta: { type: Object }
  
}, {
  timestamps: true
});

module.exports = schema;