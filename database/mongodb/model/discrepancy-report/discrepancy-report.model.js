const MongoDBModel = require('../../util/mongodb-model');

class DiscrepancyReportModel extends MongoDBModel {
  constructor() {
    super('discrepancy-report');
    this.document = {};
  }

  /**
   * 
   * @param {*} rows 
   * @param {*} args.name
   * @param {*} args.route
   * @param {*} args.type
   * @param {*} args.status
   * @returns 
   */
  generateDocument(rows, args) {
    const { name, route, type, status, filename } = args;
    this.document['name'] = filename.replace(/\.[^/.]+$/, '');
    this.document['route'] = route;
    this.document['transactions'] = rows.map(x => {
      return {
        data: x
      }
    })
    this.document['type'] = type;
    this.document['meta'] = this.generateMeta(rows, args);

    if(status) {
      this.document['status'] = status;
    }

    return this;
  }

  async readCsvData(filename) {
    const { FileReader, CSVReaderService } = require('@services');
    const filepath = `${process.env.APP_PUTH_PATH}/${filename}`;

    try {
      const { payload, error } = await CSVReaderService.readByFile(filepath, {separator: ';'});
      if(error) throw error;
      FileReader.remove(filepath);
      return {payload};
    } catch (error) {
      return {error};
    }
  }

  resetDocument() {
    this.document = {};
    return this;
  }

  generateMeta() {
    const meta = {}
    return meta;
  }
}

module.exports = DiscrepancyReportModel;