const moment = require('../../../../helpers/date');
const MongoDBModel = require('../../util/mongodb-model');

class SalesMasterModel extends MongoDBModel {
  constructor() {
    super('sales-master');
    this.document = {};
  }

  /**
   * 
   * @param {*} rows 
   * @param {*} args.groupByFields
   * @param {*} args.csvname
   * @param {*} args.childType
   * @param {*} args.type
   * @returns 
   */
  generateDocument(rows, args) {
    const { type } = args;
    this.document['key'] = rows[0]['unique_key'];
    this.document['type'] = type;
    this.document['data'] = rows;
    this.document['meta'] = this.generateMeta(rows, args);
    return this;
  }

  resetDocument() {
    this.document = {};
    return this;
  }

  generateMeta(rows, {groupByFields, csvname}) {
    const lines = rows.filter(x => x.LINE_NUMBER !== '');
    
    const meta = {
      csvname,
      group_by_fields: groupByFields,
      total_lines: lines.length
    }

    return meta;
  }

  generateKey(item, fields) {
    let key = '';
    fields = Array.isArray(fields) ? fields : [];
    for (const field of fields) {
      if(item[field]) {
        key = key.concat(item[field]);
      }
    }
    return key;
  }

  async fetchData() {
    try {
      const { CSVReaderService } = require('@services');
      const { payload, error } = await CSVReaderService.readByUrl(process.env.APP_CEGID_SALES_REPORT_URL);
      
      // Return csv data
      if(!error) return {payload};
      
      // Throw error from reader
      throw error;
    } catch (error) {
      return {error};
    }
  }

  /**
   * Get missing records
   * @returns Get
   */
   async missingSales(projections = 'data', maxDate) {
    const query = {
      'meta.available': null,
      'data': {'$elemMatch': {'oraclecode': {$ne: 'E99'}}}, // Exclude E99 Store
      'createdAt': {'$gte': new Date('September 01, 2022')} // Get only sales aster September 01.
    };

    if(maxDate) {
      const today = moment();

      if(maxDate == 1) {
        query.createdAt['$lte'] = moment(`${today.format('MMDDYY')}0100`, 'MMDDYYHHmm').toDate();
      }

      if(maxDate == 14) {
        query.createdAt['$lte'] = moment(`${today.format('MMDDYY')}1400`, 'MMDDYYHHmm').toDate();
      }
    }

    const result = await this.find(query, '', {limit: 0});
    return result;
  }

  async getListViewColumns(records) {
    const rows = records.map(x => {
      const metadata = x.data[0];
      const row = {
        key: x.meta.header_key,
        division: metadata.salesdivision,
        store: metadata.oraclecode,
        lines: x.data.length,
        missing: !x.meta.available ? 'true' : '',
        updatedAt: moment(x.updatedAt).format('MMM DD, YYYY hh:mm A z'),
        createdAt: moment(x.createdAt).format('MMM DD, YYYY hh:mm A z'),
        updatedAtRaw: x.updatedAt,
        createdAtRaw: x.createdAt
      };

      return row;
    });

    return rows;
  }
}

module.exports = SalesMasterModel;