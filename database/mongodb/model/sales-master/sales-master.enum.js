const ENUM = require('../../util/mongodb-enum');

const STATUS_MODEL = {
  enum: ENUM.status_arr,
  default: ENUM.status_obj.queued
};

module.exports = {
  status: STATUS_MODEL
};