const mongoose = require('mongoose');
const enumlist = require('./sales-master.enum');

// Schema
const schema = mongoose.Schema({
  data: {
    type: [Object]
  },
  status: {
    interface: {
      type: String,
      enum: enumlist.status.enum,
      default: enumlist.status.default
    },
    external: {
      type: String,
      enum: enumlist.status.enum,
      default: enumlist.status.default
    },
  },
  meta: {type: Object}
}, {
  timestamps: true
});

module.exports = schema;