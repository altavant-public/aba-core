const mongoose = require('mongoose');

/**
 * Status Guideline
 * 
 * "QUEUED":
 * - Initial status of the record.
 * - Don't proceed to header generation if this status exist upon checking from background task
 * 
 * "LINES_GENERATED":
 * - Status if lines is created in staging-order-lines
 * - Don't proceed to header generation if this status exist upon checking from background task
 * 
 * "LINES_EXPORTED":
 * - Status if lines already exported as an interface file
 */
const schema = mongoose.Schema({
  key: { // Identifier of PO / Transfer
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  type: {type: String, enum: ['alf','trv']}, // Transaction type if it's PO or Transfer
  meta: {
    status: {type: String, required: true, default: 'queued', enum: ['queued', 'lines_generated', 'lines_exported', 'lines_exported_with_oracle_error', 'failed', 'processing']},
    cegidIdentifier: {type: String, required: true},
    error: {type: String},
    note: {type: String}
  },
  data: {type: Object},
  valid: {type: Boolean, default: false}
}, {
  timestamps: true
});

// Indexes
schema.index({ key: 1 });
schema.index({ type: 1 });
// schema.index({ valid: 1 });
schema.index({ 'meta.totalLines': 1 });
schema.index({ 'meta.status': 1 });
// schema.index({ 'filters.docNumber': 1, 'filters.store': 1 });
// schema.index({ createdAt: -1 });
// schema.index({ updatedAt: -1 });

/**
 * Set index to autodelete this record
 */
// const secondsInADay = 86400;
const secondsPerMinute = 60;
const duration = (secondsPerMinute * 3.5); // Delete records older than 3.5 minutes
schema.index({ createdAt: 1 }, { expireAfterSeconds: duration, partialFilterExpression: { 'meta.status': { $eq: 'lines_exported' } } });

module.exports = schema;