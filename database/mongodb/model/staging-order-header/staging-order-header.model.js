const moment = require('../../../../helpers/date');
const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {

  constructor() {
    super('staging-order-header');
    this.document = {};
  }

  async getListViewColumns(records) {
    const getData = (data) => {
      let display = '';ac

      for (const key in data) {
        display += `${key} = ${data[key]} <br>`;
      }

      return display
    };

    const rows = records.map(x => {
      const metadata = x.data[0];
      const row = {
        '_id': x._id,
        'key': x.key,
        'meta.status': x.meta.status,
        'type': x.type,
        'data': getData(x.data),
        'valid': x.valid,
        'updatedAt': moment(x.updatedAt).format('MMM DD, YYYY hh:mm A z'),
        'createdAt': moment(x.createdAt).format('MMM DD, YYYY hh:mm A z')
      };

      return row;
    });

    return rows;
  }
}

module.exports = Model;