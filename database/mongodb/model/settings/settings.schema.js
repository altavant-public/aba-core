const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  type: {type: String},
  meta: {type: Object}
}, {
  timestamps: true
});

module.exports = schema;