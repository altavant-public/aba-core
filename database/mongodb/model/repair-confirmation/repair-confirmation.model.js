const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('repair-confirmation');
    this.document = {};
  }

  formatDocument(data) {
    const document = {
      id: data.repair_order_number,
      metadata: JSON.stringify(data)
    };

    return document;
  }
}

module.exports = new Model();