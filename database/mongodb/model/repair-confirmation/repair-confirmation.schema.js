const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  metadata: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    enums: ['queued', 'success', 'failed', 'done', 'processing'],
    default: 'queued'
  },
  result: {
    type: Object
  }
}, {
  timestamps: true
});

// Indexes
schema.index({ id: 1 });
schema.index({ status: 1, result: 1 });

module.exports = schema;