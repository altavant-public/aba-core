const mongoose = require('mongoose');

// Schema
const _schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  header: {type: mongoose.Schema.Types.ObjectId, ref: 'outbound_header'},
  identifier: {
    type: String
  },
  request: {
    type: String
  },
  data: {
    type: Object
  },
  referenceNo: { // Depreciated
    type: String
  },
  status: {
    type: String,
    required: true,
    default: '0',
    enum: ['0','1','2','3']
  },
  origin: { // Depreciated
    type: String
  },
  route: {
    type: String
  },
  comment: {
    type: String
  },
  response: {
    success: {type: String},
    error: {type: String},
    default: {success: "", error: ""}
  },
  meta: {type: Object}
}, {
  timestamps: true
});

// Indexes
_schema.index({ route: 1, comment: 1, createdAt: 1  });
_schema.index({ identifier: 1 });

// Model
const _model = mongoose.model('outbound', _schema);

// Controller
const _controller = require('./util/controller')(_model);

// Aggregate helpers
const _groupObjects = {
  id: { '$last': '$id'},
  mongo_id: { '$last': '$_id'},
  identifier: { '$last': '$identifier'},
  response: { '$last': '$response'},
  data: { '$last': '$data'},
  status: { '$last': '$status'},
  route: { '$last': '$route'},
  comment: { '$last': '$comment'},
  meta: { '$last': '$meta'},
  createdAt: { '$last': '$createdAt'}
};

module.exports = {
  OutboundModel: _model,
  OutboundSchema: _schema,
  OutboundController: _controller,
  OutboundGroupObjects: _groupObjects
}