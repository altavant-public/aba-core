const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('outbound');
    this.document = {};
  }
}

module.exports = new Model();