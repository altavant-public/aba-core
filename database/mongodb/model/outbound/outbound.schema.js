const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  header: {type: mongoose.Schema.Types.ObjectId, ref: 'outbound_header'},
  identifier: {
    type: String
  },
  request: {
    type: String
  },
  data: {
    type: Object
  },
  referenceNo: { // Depreciated
    type: String
  },
  status: {
    type: String,
    required: true,
    default: '0',
    enum: ['0','1','2','3']
  },
  origin: { // Depreciated
    type: String
  },
  route: {
    type: String
  },
  comment: {
    type: String
  },
  response: {
    success: {type: String},
    error: {type: String},
    default: {success: "", error: ""}
  },
  meta: {type: Object},
  archivedAt: {type: Date},
}, {
  timestamps: true
});

// Indexes
schema.index({ comment: 1, createdAt: 1  });
schema.index({ identifier: 1 });

module.exports = schema;