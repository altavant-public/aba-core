const helpers = {};

helpers.groupObjects = {
  id: { '$last': '$id'},
  mongo_id: { '$last': '$_id'},
  identifier: { '$last': '$identifier'},
  response: { '$last': '$response'},
  data: { '$last': '$data'},
  status: { '$last': '$status'},
  route: { '$last': '$route'},
  comment: { '$last': '$comment'},
  meta: { '$last': '$meta'},
  createdAt: { '$last': '$createdAt'}
};

module.exports = helpers;