const mongoose = require('mongoose');
const crypto = require('crypto');
const hashGenerator = (password, salt) => {
  return crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
};

// Schema
const _schema = mongoose.Schema({
  username: {type: String, unique: true, trim: true, sparse: true, required: true},
  email: {type: String, unique: true, trim: true, sparse: true},
  auths: [{
    authStrategy: {
      required: true,
      type: String,
      enum: [
        'local'
      ],
      trim: true
    },
    authKey: {type: String},
    authToken: {type: String}
  }],
  name: {
    first: {type: String},
    middle: {type: String},
    last: {type: String,}
  },
  systemRole: {
    type: String,
    enum: [
      'user',
      'admin',
      'it',
      'altavant_support',
      'webmaster'
    ],
    required: true,
    trim: true,
    default: 'user'
  },
  token: {
    resetPassword: {type: String}
  },
  meta: {type: Object},
  permissions: [],
  active: {type: Boolean, default: true},
  hash: {type: String},
  salt: {type: String, default: crypto.randomBytes(16).toString('hex')},
  isTemporaryPassword: {type: Boolean, default: true},
  disabled: {type: Boolean, default: false}
}, {
  timestamps: true
});

/**
 * Method to check the entered password is correct or not
 * valid password method checks whether the user
 * password is correct or not
 * It takes the user password from the request 
 * and salt from user database entry
 * It then hashes user password and salt
 * then checks if this generated hash is equal
 * to user's hash in the database or not
 * If the user's hash is equal to generated hash 
 * then the password is correct otherwise not
 */
 _schema.methods.validPassword = function(password) {
  return this.hash === hashGenerator(password, this.salt);
};

// Model
const _model = mongoose.model('user', _schema);

// Controller
const _controller = require('./util/controller')(_model);

_controller.hashGenerator = hashGenerator;
_controller.register = async ({username, password, systemRole, division}) => {
  return new Promise((resolve) => {
    try {
      const newUser = new _model();
      newUser.username = username;
      newUser.hash = hashGenerator(password, newUser.salt);

      if(systemRole) {
        newUser.systemRole = systemRole;
      }

      if(division) {
        newUser.meta = {division: division};
      }

      newUser.save((err) => {
        if (err) {
          resolve({success: false, error: err.message});
        }
        else {
          resolve({success: true, payload: newUser});
        }
      });
    } catch (err) {
      resolve({success: false, error: err.message});
    }
  });
}

module.exports = {
  UserModel: _model,
  UserSchema: _schema,
  UserController: _controller
}