'use strict';

const DEFAULT_GET_OPTION = {skip: 0, limit: 10, sorts: '-createdAt'};

module.exports = (_model) => {
  const controller = {};
  
  controller.countDocuments = async (filters) => {
    try {
      const result = await _model
      .countDocuments(filters)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.getHistory = async (groupObjects, query = {}, options = {sort: {createdAt: -1}}) => {
    try {
      const result = await _model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .sort({createdAt: -1})
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.getList = async (groupObjects, query = {}, options = {}) => {
    try {
      const result = await _model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .sort(options.sort || {createdAt: -1})
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.groupBy = async (groupObjects, query = {}, options = {}) => {
    try {
      const result = await _model
      .aggregate()
      .group(groupObjects)
      .match(query)
      .sort(options.sort || {createdAt: -1})
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.groupByForCSV = async (groupObjects, query = {}) => {
    try {
      const result = await _model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.groupByCount = async (groupObjects, query = {}) => {
    try {
      const key = 'total';
      const result = await _model
      .aggregate()
      .group(groupObjects)
      .match(query)
      .allowDiskUse(true)
      .count(key);

      let total = 0;
      if(result && result.length > 0) {
        total = result[0][key];
      }
      
      return {success: true, payload: total};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.getAll = async (query = {}, options = {skip: 0, limit: 10}, sorts = '-createdAt', projection) => {
    try {
      const result = await _model.find(query, projection)
      .sort(sorts)
      .skip(options.skip || 0)
      .limit(options.limit || 0)
      .lean()
      .allowDiskUse(true);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.find = async (query = {}, projection, customOptions = {}) => {
    try {
      const options = {...DEFAULT_GET_OPTION, ...customOptions};
      const result = await _model.find(query, projection)
      .sort(options.sorts)
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .lean()
      .allowDiskUse(true);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.getOne = async (query = {}, projection, options = {lean: true}) => {
    try {
      const result = await _model.findOne(query, projection, options);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.findById = async (id, projection, options = {lean: true}) => {
    try {
      const result = await _model.findById(id, projection, options);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.findOneAndUpdate = async (filter, payload, options = {}) => {
    return new Promise((resolve) => {
      try {
        _model.findOneAndUpdate(filter, payload, {runValidators: true, ...options}, (err, data) => {
          if(!err) {
            resolve({success: true, payload: data});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.updateOrCreate = async (filters, payload) => {
    try {
      const response = await _model.findOneAndUpdate(filters, payload, {
        new: true,
        upsert: true,
        rawResult: true // Return the raw result from the MongoDB driver
      });
      return {success: true}
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  controller.create = async (payload) => {
    return new Promise((resolve) => {
      try {
        _model.create(payload, (err) => {
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.insertMany = async (payload) => {
    return new Promise((resolve) => {
      try {
        _model.insertMany(payload, (err) => {
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.updateStatus = async (filter, payload) => {
    return new Promise((resolve) => {
      try {
        _model.updateOne(filter, payload, {new: true, strict: false}, (err, data) => {
          
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.updateOne = async (filter, payload) => {
    return new Promise((resolve) => {
      try {
        _model.updateOne(filter, payload, {new: true, strict: false}, (err, data) => {
          
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.update = async (filter, payload, options) => {
    return new Promise((resolve) => {
      try {
        _model.updateMany(filter, payload, options, (err, data) => {
          
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  controller.deleteOne = async (filters) => {
    try {
      await _model.deleteOne(filters);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  controller.exists = async (filter) => {
    try {
      return await _model.exists(filter);
    } catch (err) {
      return false;
    }
  }

  controller.findByIdAndDelete = async (id) => {
    return new Promise((resolve) => {
      try {
        _model.findByIdAndDelete(id, (err) => {
          if(!err) {
            resolve({success: true});
          } else {
            resolve({success: false, error: err});
          }
        });
      } catch (err) {
        resolve({success: false, error: err.message});
      }
    });
  }

  return controller;
}