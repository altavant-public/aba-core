const mongoose = require('mongoose');

// Schema
const _schema = mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  header: {type: mongoose.Schema.Types.ObjectId, ref: 'inbound_header'},
  identifier: {
    type: String
  },
  request: {
    type: String
  },
  data: {
    type: Object
  },
  referenceNo: { // Depreciated
    type: String
  },
  status: {
    type: String,
    required: true,
    default: '0',
    enum: ['0','1','2','3']
  },
  origin: { // Depreciated
    type: String
  },
  route: {
    type: String
  },
  comment: {
    type: String
  },
  response: {
    success: {type: String},
    error: {type: String},
    default: {success: "", error: ""}
  },
  meta: {type: Object}
}, {
  timestamps: true
});

// Indexes
_schema.index({request: 'text'});

// Model
const _model = mongoose.model('inbound', _schema);

// Controller
const _controller = require('./util/controller')(_model);

// Aggregate helpers
const _groupObjects = {
  id: { '$last': '$id'},
  mongo_id: { '$last': '$_id'},
  identifier: { '$last': '$identifier'},
  response: { '$last': '$response'},
  data: { '$last': '$data'},
  status: { '$last': '$status'},
  route: { '$last': '$route'},
  comment: { '$last': '$comment'},
  meta: { '$last': '$meta'},
  header: { '$last': '$header'},
  createdAt: { '$last': '$createdAt'}
};

module.exports = {
  InboundModel: _model,
  InboundSchema: _schema,
  InboundController: _controller,
  InboundGroupObjects: _groupObjects
}