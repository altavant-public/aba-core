const helpers = {};

/**
 * Function to generate fields to populate when Aramex ASN api call is successful.
 * @param {object} ASNResult Response from Aramex
 * @returns 
 */
helpers.fieldToUpdateOnAramexASNimport = (ASNResult) => {
  const updates = {};

  if(ASNResult.Status == 'SUCCESS') {
    updates['$set'] = {'status': 'asn_pushed', 'data.aramexAsn': ASNResult.Reference};
    updates['$push'] = {'transaction_log': {payload: `ASN created successfully. Reference# ${ASNResult.Reference}`}};
  } else {
    const errorLog = 'ErrorDescription' in ASNResult
    ? `Error Importing ASN to Aramex. Error: ${ASNResult.ErrorDescription}`
    : `Error Importing ASN to Aramex. Error: ${JSON.stringify(ASNResult)}`
    updates['$push'] = {'transaction_log': {success: false, payload: errorLog}};
  }
  return updates;
}

module.exports = helpers;