module.exports = {
  /**
   * 0 = failed
   * 1 = successful
   * 2 = incomplete
   * 3 = processing
   */
  status: ['0','1','2','3']
};