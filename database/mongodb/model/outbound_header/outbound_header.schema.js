const mongoose = require('mongoose');
const enumlist = require('./outbound_header.enum');

// Schema
const schema = mongoose.Schema({
  identifier: {
    type: String
  },
  data: {
    type: Object
  },
  comment: {
    type: String
  },
  route: {
    type: String
  },
  status: {
    type: String,
    required: true,
    default: '3',
    enum: enumlist.status
  },
  response: {
    success: {type: String},
    error: {type: String},
    default: {success: '', error: ''}
  },
  meta: {type: Object}
}, {
  timestamps: true
});

module.exports = schema;