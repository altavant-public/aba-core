const MongoDBModel = require('../../util/mongodb-model');

class OutboundHeaderModel extends MongoDBModel {
  constructor() {
    super('outbound_header');
  }

  /**
   * Function to update transfer when Aramex ASN api call is successful.
   * @param {string} internalReference 
   * @param {object} ASNResult 
   */
  async updateOnAramexASNimport(internalReference, ASNResult) {
    const updates = this.helper.fieldToUpdateOnAramexASNimport(ASNResult);
    const result = await this.updateOne({identifier: internalReference}, updates);
    return result;
  }

  /**
   * To update transfer document when transfer received is sent by Aramex via CSV
   * @param {string} param0.reference Transfer internal reference 
   * @param {string} param0.error Transfer received error from cegid
   * @param {object} param0.request Request from Aramex
   * @param {string} param0.transferReceivedCSV Generate csv file when aramex call transfer receive API.
   * @returns 
   */
  async updateDocumentOnCSVReception ({reference, error, request, transferReceivedCSV}) {
    try {
      const updates = {};
      if(!error) {
        updates['$set'] = {'status': this.enum['status']['received_by_aramex'], 'data.reception_request': request, 'meta.transfer_received_csv': transferReceivedCSV};
        updates['$push'] = {'transaction_log': {payload: 'Transfer received by Aramex'}};
      } else {
        updates['$set'] = {'data.reception_request': request};
        updates['$push'] = {'transaction_log': {success: false, payload: error}};
      }
      const result = await this.updateOne({identifier: reference}, updates);
      return result;
    } catch (error) {
      // TODO: Email this error to webmaster
    }
  };

  /**
   * !!! USED IF RECEIVING IS via CSV. FOR CEGID V18 !!!
   * To update transfer document when transfer received is successful from Cegid.
   * @param {string} csvname Generated csv filename
   * @param {string} treDocNumber Transfer Receive document number from cegid
   */
  async updateSuccessfulReception (csvname, treDocNumber) {
    try {
      const updates = {
        $set: {
        'status': this.enum['status']['received']
        },
        $push: {
          'transaction_log': {success: true, payload: 'Successfully received in Cegid'}
        }
      };
  
      if(treDocNumber) {
        updates['$push']['data.documents'] = {number: treDocNumber, type: 'ReceivedTransfer'};
      }
  
      const query = {
        'meta.transfer_received_csv': csvname
      };
      await this.update(query, updates);
    } catch (error) {
      throw error;
    }
  }
  
  /**
   * !!! USED IF RECEIVING IS via CSV. FOR CEGID V18 !!!
   * To update transfer document when transfer received is rejected from Cegid.
   * @param {string} param0.filename Generated csv filename
   * @param {string} param0.log Transfer error from Cegid.
   */
  async updateRejectedReception (event) {
    try {
      const updates = {
        $push: {
          'transaction_log': {success: false, payload: event.log}
        }
      };
      const query = {
        'meta.transfer_received_csv': event.filename
      };
      await this.update(query, updates);
    } catch (error) {
      throw error;
    }
  }

  /**
   * !!! USED IF RECEIVING IS via API. FOR CEGID V19 !!!
   * To update transfer document when transfer received is sent by Aramex via API
   * @param {string} param0.reference Transfer internal reference 
   * @param {string} param0.error Transfer received error from cegid
   * @param {object} param0.request Request from Aramex
   * @param {object} param0.document Transfer received document. Ex: {number: {document number}, type: 'ReceivedTransfer'}
   * @returns 
   */
  async updateDocumentOnAPIReception ({reference, error, request, document}) {
    try {
      const updates = {};
      if(!error) {
        updates['$set'] = {'status': this.enum['status']['received'], 'data.reception_request': request};
        updates['$push'] = {'data.documents': document, 'transaction_log': {payload: 'Successfully received in Cegid.'}};
      } else {
        updates['$set'] = {'data.reception_request': request};
        updates['$push'] = {'transaction_log': {success: false, payload: error}};
      }
      const result = await this.updateOne({identifier: reference}, updates);
      return result;
    } catch (error) {
      // TODO: Email this error to webmaster
    }
  }
}

module.exports = new OutboundHeaderModel();