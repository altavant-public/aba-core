const MongoDBModel = require('../../util/mongodb-model');
const SalesStagingReportModel = require('../../../../util/mappings/models/sales-staging-report-generator.model');

class StagingSalesModel extends MongoDBModel {
  constructor() {
    super('staging-sales');
    this.document = {};
  }

  /**
   * 
   * @param {*} rows 
   * @param {*} args.groupByFields
   * @param {*} args.csvname
   * @param {*} args.childType
   * @param {*} args.type
   * @returns 
   */
  generateDocument(rows, args) {
    const { type } = args;
    this.document['key'] = rows[0]['unique_key'];
    this.document['type'] = type;
    this.document['data'] = rows;
    this.document['meta'] = this.generateMeta(rows, args);
    return this;
  }

  resetDocument() {
    this.document = {};
    return this;
  }

  generateMeta(rows, {groupByFields, csvname}) {
    const sales = rows.filter(x => (x.LINE_NUMBER && x.ITEM_CODE));
    const payments = rows.filter(x => (!x.LINE_NUMBER && !x.ITEM_CODE));
    
    const meta = {
      csvname,
      group_by_fields: groupByFields,
      sales: sales,
      payments: payments,
      sales_total_lines: sales.length,
      payment_total_lines: payments.length
    }

    return meta;
  }

  /**
   * Get a record needed to validate
   * @returns 
   */
  async forValidationRecord(key) {
    // const prevDate = new Date(Date.now() - 12 * 60 * 60 * 1000); // Date from past 12 hours
    // const query = {
    //   'meta.status': null,
    //   '$or': [{valid: null}, {valid: false}],
    //   'createdAt': {'$gte': prevDate}
    // }

    const query = {
      'meta.status': null,
      'valid': null
    }

    if(key) {
      query['key'] = key;
    }

    const result = await this.getOne(query, '', {sorts: 'createdAt'});
    return result;
  }

  /**
   * Get a record needed to re-validate manually
   * @returns 
   */
   async forReValidationRecord(key) {
    const query = {
      'meta.status': null,
      '$or': [{valid: null}, {valid: false}]
    }

    if(key) {
      query['key'] = key;
    }

    const result = await this.getOne(query, '', {sorts: 'createdAt'});
    return result;
  }

  /**
   * Reset records that has no_sales_data error
   * @returns 
   */
  async resetNoSalesDataRecords() {
    const query = {
      'meta.status': 'done',
      'valid': false,
      'meta.error.sale_lines.key': 'no_salesmaster_data'
    };

    const updates = {
      $unset: {
        'valid': 1,
        'meta.status': 1
      }
    };

    await this.update(query, updates, {multi: true});
  }

  /**
   * Reset all failed records
   * @returns 
   */
   async resetFailedRecords() {
    const query = {
      'meta.status': 'done',
      'valid': false
    };

    const updates = {
      $unset: {
        'valid': 1,
        'meta.status': 1
      }
    };

    await this.update(query, updates, {multi: true});
  }

  /**
   * Update record that is going to interface/push
   * @returns 
   */
   async updateToProcessing(key) {
    const query = { key };
    const updates = {
      $set: {
        'meta.status': 'processing'
      }
    };

    await this.update(query, updates, {multi: true});
  }

  /**
   * Update record that is finished to interface/push
   * @returns 
   */
   async updateToFinish(key) {
    const query = { key };
    const updates = {
      $set: {
        'meta.status': 'success'
      }
    };

    await this.update(query, updates, {multi: true});
  }

  /**
   * Get a valid record
   * @returns
   */
  async validRecord() {
    const query = {
      'meta.status': 'done',
      'valid': true
    }

    const result = await this.getOne(query);
    return result;
  }

  /**
   * Query to get the recent valid transaction.
   * This is to identify if "intefacepush-sale-staging" event is safe
   * to call in background task
   * @returns
   */
   async readyToInterfaceValidRecord() {
    const query = {
      'valid': true,
      'meta.status': {$in: ['done', 'success']}
    };

    const result = await this.getOne(query, '', { sorts: '-updatedAt' });
    return result;
  }

  /**
   * Get failed records
   * @returns Get
   */
   async failedRecords() {
    // const query = {
    //   '$or': [
    //     {'valid': false},
    //     {'valid': null, 'meta.status': null, 'createdAt': {'$lte': new Date()}},
    //     {'valid': null, 'meta.status': 'done', 'createdAt': {'$lte': new Date()}}
    //   ]
    // };

    const query = {
      'meta.status': 'done',
      'valid': false
    };

    const result = await this.find(query, '', {limit: 0});
    return result;
  }

  async getListViewColumns(records) {
    const report = new SalesStagingReportModel();
    await report.generateRows(records, true);
    return report.rows;
  }
}

module.exports = StagingSalesModel;