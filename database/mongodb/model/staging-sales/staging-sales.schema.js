const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  key: {
    type: String,
    required: true,
    unique: true,
    sparse: true
  },
  type: {type: String}, // Child type of the sale
  meta: {type: Object},
  data: [{type: Object}],
  valid: {type: Boolean, default: false}
}, {
  timestamps: true
});

module.exports = schema;