const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  filename: {
    type: String
  },
  date: {
    type: String
  },
  log: {
    type: String
  },
  status: {
    type: String,
    enum: ['success','rejected','processing', '1', '0']
  },
  meta: {type: Object}
}, {
  timestamps: true
});

// Indexes
schema.index({ filename: 1  });
schema.index({ status: 1 });

module.exports = schema;