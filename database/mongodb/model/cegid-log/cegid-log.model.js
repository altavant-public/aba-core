const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('cegid-log');
    this.document = {};
  }
}

module.exports = new Model();