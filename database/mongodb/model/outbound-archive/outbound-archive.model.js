const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('outbound-archive');
    this.document = {};
  }
}

module.exports = new Model();