const mongoose = require('mongoose');
const CegidLogMeta = {
  status: {
    success: '1',
    rejected: '0',
    processing: 'processing'
  }
}

// Schema
const _schema = mongoose.Schema({
  filename: {
    type: String
  },
  date: {
    type: String
  },
  log: {
    type: String
  },
  status: {
    type: String,
    enum: ['success','rejected','processing', '1', '0']
  },
  meta: {type: Object}
}, {
  timestamps: true
});

// Indexes
_schema.index({request: 'text'});

// Model
const _model = mongoose.model('cegid_log', _schema);

// Controller
const _controller = require('./util/controller')(_model);

// Aggregate helpers
// const _groupObjects = {
//   id: { '$last': '$id'},
//   mongo_id: { '$last': '$_id'},
//   identifier: { '$last': '$identifier'},
//   response: { '$last': '$response'},
//   data: { '$last': '$data'},
//   status: { '$last': '$status'},
//   route: { '$last': '$route'},
//   comment: { '$last': '$comment'},
//   meta: { '$last': '$meta'},
//   createdAt: { '$last': '$createdAt'}
// };

module.exports = {
  CegidLogModel: _model,
  CegidLogSchema: _schema,
  CegidLogController: _controller,
  CegidLogMeta: CegidLogMeta,
  // InboundGroupObjects: _groupObjects
}