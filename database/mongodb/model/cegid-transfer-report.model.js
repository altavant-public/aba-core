const mongoose = require('mongoose');
const STATUS_ENUM = ['processing', '1', '0'];

// Schema
const _schema = mongoose.Schema({
  data: {
    type: Object
  },
  status: {
    interface: {
      type: String,
      enum: STATUS_ENUM,
      default: 'processing'
    },
    external: {
      type: String,
      enum: STATUS_ENUM,
      default: 'processing'
    },
  },
  meta: {type: Object}
}, {
  timestamps: true
});

// Model
const _model = mongoose.model('cegid_transfer_report', _schema);

// Controller
const _controller = require('./util/controller')(_model);

module.exports = {
  CegidTransferReportModel: _model,
  CegidTransferReportSchema: _schema,
  CegidTransferReportController: _controller
}