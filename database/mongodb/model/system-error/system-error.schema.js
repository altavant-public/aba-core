const mongoose = require('mongoose');

// Schema
const schema = mongoose.Schema({
  type: {type: String},
  error: {type: String},
  stack: {type: String},
  meta: {type: Object}
}, {
  timestamps: true
});

/**
 * Set index to autodelete this record
 */
const secondsInADay = 86400;
const daysOfDeletion = (secondsInADay * 10); // Delete records older than 180 days (6 months);
schema.index({ createdAt: 1 }, { expireAfterSeconds: daysOfDeletion });

module.exports = schema;