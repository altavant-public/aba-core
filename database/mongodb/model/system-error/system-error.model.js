const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('system-error');
    this.document = {};
  }

  /**
   * Update record that is failed
   * @returns 
   */
  async markAlertAsSent(keys) {
    const query = { _id: { $in: keys} };
    const updates = {
      $set: {
        'meta.sent': true
      }
    };

    const result = await this.update(query, updates, {multi: true});
    return result;
  }
}

module.exports = new Model();