const MongoDBModel = require('../../util/mongodb-model');

class Model extends MongoDBModel {
  constructor() {
    super('queue');
    this.document = {};
  }
}

module.exports = new Model();