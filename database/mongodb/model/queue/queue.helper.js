const helpers = {};

helpers.STATUS = {
  QUEUED: 'queued',
  SUCCESS: 'success',
  FAILED: 'failed',
  PROCESSING: 'processing'
};

module.exports = helpers;