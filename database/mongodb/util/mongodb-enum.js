const STATUS_LIST = ['queued', 'processing', 'success', 'failed', 'done'];

module.exports = {
  status_arr: STATUS_LIST,
  status_obj: {
    queued: 'queued',
    processing: 'processing',
    success: 'success',
    failed: 'failed',
    done: 'done'
  },
  filters: {
    type: {
      enum: ['rejected-outbound-checker']
    },
    status: {
      enum: STATUS_LIST
    }
  }
};