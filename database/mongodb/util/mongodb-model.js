const MongoDBQuery = require('./mongodb-query')

/**
 * Main model of the existing collections.
 * 
 * @param {Object} enum Enum values for by field
 * @param {Object} helper reusable functions for the model and custom query.
 */
class MongoDBModel extends MongoDBQuery {
  constants = {
    STATUS_MODEL_ARR: ['queued', 'processing', 'success', 'failed', 'done'],
    STATUS_MODEL_OBJ: {
      queued: 'queued',
      processing: 'processing',
      success: 'success',
      failed: 'failed',
      done: 'done'
    }
  };

  constructor(collection) {
    super(collection);
    this.helper = this.getHelper(collection);
    this.enum = this.getEnum(collection);
    this.schema = this.getSchema(collection);
  }

  getHelper(collection) {
    try {
      return require(`../model/${collection}/${collection}.helper`);
    } catch (error) {
      return {};
    }
  }

  getEnum(collection) {
    try {
      return require(`../model/${collection}/${collection}.enum`);
    } catch (error) {
      return {};
    }
  }
}

module.exports = MongoDBModel;