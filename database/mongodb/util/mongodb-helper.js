'use strict'

const { UtilHelper } = require('@helpers');

const helper = {};

helper.defaultFilter = (filters) => {
  const query = {};

  if(filters.key) {
    query['key'] = { $regex: '.*' + filters.key + '.*' };
  }

  if(filters?.valid) {
    let values = UtilHelper.queryValueFormatter(filters.valid);
    if(values.length) {
      query['valid'] = {'$in': values};
    } 
  }

  if(filters?.status) {
    let values = UtilHelper.queryValueFormatter(filters.status);
    if(values.length) {
      query['meta.status'] = {'$in': values};
    } 
  }
  return query;
}

helper.defaultOptions = (customOptions) => {
  const options = {
    limit: parseInt(customOptions.limit) || 10,
    sorts: customOptions?.sort_by || '-updatedAt'
  };

  return options;
}

module.exports = helper;