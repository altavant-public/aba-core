const GlobalInitializer = require('../../../core/global-initializer');
GlobalInitializer();

try {
  // Initiate globals
  const { MongoDBConnect } = require('@core');
  MongoDBConnect();
} catch (error) {
  console.error(error);
}