const mongoose = require('mongoose');
const DEFAULT_GET_OPTION = {skip: 0, limit: 10, sorts: '-createdAt'};

/**
 * User defined functions of mongoDB operations with default response Object.
 * 
 * @param {Object} model Enum values for by field
 * @return {Object} {success: boolean, payload: collection result, error: collection error};
 */
class MongoDBQuery {

  modelPath = '../model';

  constructor(modelFileName) {
    this.model = mongoose.model(this.getCollectionName(modelFileName), this.getSchema(modelFileName));
  }

  getCollectionName(name) {
    name = name.replace(/\s+/g, '-');
    name = name.replace(/-/g, '_');
    name = name.toLocaleLowerCase();

    return name;
  }

  getSchema(collection) {
    return require(`${this.modelPath}/${collection}/${collection}.schema`);
  }

  async countDocuments(filters) {
    try {
      const result = await this.model
      .countDocuments(filters)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async getHistory(groupObjects, query = {}, options = {sort: {createdAt: -1}}) {
    try {
      const result = await this.model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .sort({createdAt: -1})
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async getList(groupObjects, query = {}, options = {}) {
    try {
      const result = await this.model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .sort(options.sort || {createdAt: -1})
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async groupBy(groupObjects, query = {}, options = {}) {
    try {
      const result = await this.model
      .aggregate()
      .group(groupObjects)
      .match(query)
      .sort(options.sort || {createdAt: -1})
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async groupByForCSV(groupObjects, query = {}) {
    try {
      const result = await this.model
      .aggregate()
      .match(query)
      .group(groupObjects)
      .allowDiskUse(true);
      
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }
  
  async groupByCount(groupObjects, query = {}) {
    try {
      const key = 'total';
      const result = await this.model
      .aggregate()
      .group(groupObjects)
      .match(query)
      .allowDiskUse(true)
      .count(key);

      let total = 0;
      if(result && result.length > 0) {
        total = result[0][key];
      }
      
      return {success: true, payload: total};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async getAll(query = {}, options = {skip: 0, limit: 10, lean: true}, sorts = '-createdAt', projection) {
    try {
      const result = await this.model.find(query, projection)
      .sort(sorts)
      .skip(options.skip || 0)
      .limit(options.limit || 10)
      .lean()
      .allowDiskUse(true);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async find(query = {}, projection, customOptions = {}) {
    try {
      const options = {...DEFAULT_GET_OPTION, ...customOptions};
      const result = await this.model.find(query, projection)
      .sort(options.sorts)
      .skip(options.skip || 0)
      .limit(options.limit)
      .lean()
      .allowDiskUse(true);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async getOne(query = {}, projection, customOptions = {}) {
    try {
      const options = {sorts: '-createdAt', ...customOptions};
      const result = await this.model.findOne(query)
      .select(projection)
      .sort(options.sorts)
      .lean()
      .allowDiskUse(true);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async findById(id, projection, options = {lean: true}) {
    try {
      const result = await this.model.findById(id, projection, options);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async findOneAndUpdate(filter, payload, customOptions = {}) {
    const options = {runValidators: true, ...customOptions};
    try {
      const result = await this.model.findOneAndUpdate(filter, payload, options);
      return {success: true, payload: result};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async updateOrCreate(filters, payload, additionalOptions = {}) {
    try {
      const defaultOptions = { new: true, upsert: true, lean: true };
      const options = {...defaultOptions, ...additionalOptions};
      const result = await this.model.findOneAndUpdate(filters, payload, options);
      return {success: true, payload: result};
    } catch (error) {
      return {success: false, error: error.message};
    }
  }

  async create(payload) {
    try {
      await this.model.create(payload);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async save(payload) {
    try {
      const model = new this.model(payload);
      const result = await model.save();
      return {success: true, payload: result};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async insertMany(payload) {
    try {
      await this.model.insertMany(payload);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async updateOne(filter, payload) {
    try {
      await this.model.updateOne(filter, payload, {new: true, strict: false});
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async update(filter, payload, options) {
    try {
      await this.model.updateMany(filter, payload, options);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async deleteOne(filters) {
    try {
      await this.model.deleteOne(filters);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }

  async exists(filter) {
    try {
      return await this.model.exists(filter);
    } catch (err) {
      return false;
    }
  }

  async findByIdAndDelete(id) {
    try {
      await this.model.findByIdAndDelete(id);
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }
  
  async deleteMany(filters) {
    try {
      await this.model.deleteMany(filters, {multi: true});
      return {success: true};
    } catch (err) {
      return {success: false, error: err.message};
    }
  }
}

module.exports = MongoDBQuery;