'use strict'

const moment = require('moment-timezone');
moment.tz.setDefault(process.env.APP_TIMEZONE);

module.exports = moment;