'use strict'

const moment = require('./date');
const { dirname, join } = require('path');
const util = {};

util.payloadHandler = (res, code, payload) => {
  const response = {
    success: code == 200,
    payload: payload || ''
  };

  res.status(code).json(response);
};

util.serverError = (error, res, msg = 'Service not available') => {
  util.payloadHandler(res, 503, msg);
};


util.generateFilters = (query = {}) => {
  let filters = {};

  if(query.type) {
    filters['comment'] = query.type;
  }

  if(query.referenceNo) {
    filters['referenceNo'] = query.referenceNo;
  }

  if(query.status) {
    filters['status'] = {'$in': query.status.split(",")};
  }

  if(query.keyword) {
    filters['$text'] = {'$search': query.keyword};
  }

  if(query.createdFrom || query.createdTo) {
    let from = query.createdFrom || '';
    let to = query.createdTo || '';
    filters['createdAt'] = {};

    if(moment(from).isValid()) {
      filters['createdAt']['$gte'] = moment(from).toDate();
    }

    if(moment(to).isValid()) {
      filters['createdAt']['$lte'] = moment(to).toDate();
    }
  }
  return filters;
};

util.stringToJSON = (jsonString) => {
  try {
    return JSON.parse(jsonString);
  } catch (error) {
    return {};
  }
};

util.filenameFromFullPath = (fullPath) => {
  return fullPath.replace(/^.*[\\\/]/, '');
}

util.isJson = (item) => {
  item = typeof item !== "string"
    ? JSON.stringify(item)
    : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === "object" && item !== null) {
    return true;
  }

  return false;
}

util.jsonFromStr = (str) => {
  return {strData: str};
}

util.getErrorStack = (error) => {
  if(typeof error == 'object' && 'stack' in error && error.stack) {
    return error.stack.toString();
  }

  return '';
}

util.getError = (error) => {
  try {
    if(typeof error == 'object' && 'message' in error) {
      return error.message;
    }

    return error;
  } catch (err) {
    return error;
  }
}

util.randomStr = (length) => {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
 }
 return result;
}

util.queryValueFormatter = (stringValueSeparatedByComma) => {
  try {
    return stringValueSeparatedByComma.split(',').map(x => {

      // Get null value
      if(x == 'null') {
        return null;
      }
  
      // Get true boolean value
      if(x == 'true') {
        return true;
      }
  
      // Get false boolean value
      if(x == 'false') {
        return false;
      }
    
      return x;
    });
  } catch (error) {
    return [];
  }
}

/**
 * Get .env file path
 */
 util.envFileSrc = () => {
  return process.env.APP_ENVIRONMENT == 'dev' ? `${process.env.APP_PATH}/dev.env` : `${process.env.APP_PATH}/.env`;
}

util.rootDir = () => {
  const root = join(dirname(__filename), '..', '..');
  return root;
}

util.isProduction = process.env.APP_ENVIRONMENT === 'production';

util.isStaging = process.env.APP_ENVIRONMENT === 'qa';

util.isDevelopment = !['production', 'qa'].includes(process.env.APP_ENVIRONMENT);

module.exports = util;